require 'zlib'
require 'archive/tar/minitar'
require 'coderay'

class SubmissionsController < ApplicationController
  def show
    check_sub
    @user = @submission.user
    #@code = code_for open(URI.parse(@submission.file.url)), @submission.file_content_type
    @code = code_for File.open(@submission.file.path), @submission.file_content_type

  end
  def download
    check_admin
    check_sub
      send_file "/#{@submission.file.path}", filename: "#{@submission.original_filename}"#{params[:submission].created_at}.csv"
  end

  private

  Gzip = /application\/(x-)?gz/
  Tar = /application\/(x-)?tar/
  Gzip1 = /application\/(x-)?(tar-)?(gzip)?gz/
  #gh = 
  def code_for(io, mime_type)
    case mime_type
    when Gzip
      code_for Zlib::GzipReader.new(io), 'application/tar'
    when Gzip1
      code_for Zlib::GzipReader.new(io), 'application/tar'
    when Tar
      result = {}
      Archive::Tar::Minitar::Reader.open(io).each do |entry|
        result[entry.name] = html_for(entry) if entry.file? #&& entry.name !~ /^./
      end
      result.sort
    else
      { nil => html_for(io) }
    end
  end

  def html_for(io)
    CodeRay.scan(io.read, :ruby).html(:css=>:style,:line_numbers=>:table,:tab_width=>2,:bold_every=>false) 
  end
end

def check_sub
  @submission = Submission.find params[:id]
end
def check_admin
    if !current_user.admin?
      flash[:warning] = 'You may only view your own submissions'
      redirect_to root_path and return
    end
end

