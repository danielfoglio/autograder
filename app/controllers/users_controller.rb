class UsersController < ApplicationController
  before_filter :access_check, :except => [:home]
   before_filter :check_pass, :only => [:home, :index, :show]

  def edit
  end

  def update
    if current_user.admin? && @user == @viewer && !current_user.password_change
            
            if @user.update_with_password(params[:user]) 
              @user.password_change = true
              flash[:notice] = 'Admin Password change successful'
              sign_in @user, :bypass => true 
              params[:user].delete_if do |key, value| key =~ /password/ end
              @user.update_attributes(params[:user])
              @user.save
              if User.count < 3
                redirect_to semester_path and return
              else
                redirect_to root_path and return
              end
            else
              flash[:warning] = 'You must update your password!'
              render "edit" and return
            end
        
            
    elsif current_user.admin? && @user == @viewer && current_user.password_change
            
            if params[:user][:password].blank?
              params[:user].delete_if do |key, value| key =~ /password/ end
              flash[:notice] = 'Admin change successful, password was not updated.'
              @user.update_attributes(params[:user])
              @user.save!
            elsif @user.update_with_password(params[:user])
              flash[:notice] = 'Admin Password change successful'
              params[:user].delete_if do |key, value| key =~ /password/ end
              @user.update_attributes(params[:user])
              @user.save!
            else
              flash[:warning] = 'Update failed!'
              render "edit" and return
            end
          
        sign_in @user, :bypass => true
        redirect_to root_path and return
    
    elsif current_user.admin? && @user != @viewer && @user.update_attributes(params[:user])#.except(:current_password))
      @user.save
      #sign_in current_user, :bypass => true
      flash[:notice] = 'Update successful'
      redirect_to root_path
    elsif !current_user.admin? && @user.update_with_password(params[:user])  
      @user.update_attribute :password_change, true
      @user.save!
     # Sign in the user by passing validation in case his password changed
      sign_in @user, :bypass => true

      flash[:notice] = 'Password change successful'
      redirect_to root_path

    else
      flash[:warning] = 'Unsuccesful update'
      render "edit"
    end
  end

  def index
    @columns = [{:number=>0,:title=>'Last Name',:sort=>true,:field=>:last_name},
                {:number=>1,:title=>'First Name',:sort=>true,:field=>:first_name},
                #{:number=>2,:title=>'MI',:sort=>false},
                {:number=>2,:title=>'Student ID#',:sort=>true,:field=>:student_id},
                {:number=>3,:title=>'Email',:sort=>false},
                {:number=>4,:title=>'Section',:sort=>true,:field=>:section},
                {:number=>5,:title=>'Last Submission',:sort=>true,:method=>:last_submission,:dir=>'desc'},
                {:number=>6,:title=>' ',:sort=>false}, {:number=>7,:title=>' ',:sort=>false}]

    if params[:sort]
      sortcol = params[:sort].to_i
      sortcol = @columns[sortcol]
      sortdir = params[:dir] ? params[:dir].downcase : sortcol[:dir] ? sortcol[:dir] :'asc'
      sortcol[:class] = "sorted #{sortdir}"
      sortcol[:dir] = sortdir == 'desc' ? 'asc' : 'desc'
    end

    if sortcol && sortcol[:sort] && sortcol[:method]
      @users = User.all.sort_by(&sortcol[:method])
      @users = @users.reverse if sortdir == 'desc'
    elsif sortcol && sortcol[:sort] && sortcol[:field]
      ordering = "#{sortcol[:field]} #{sortdir}"
      @users = User.order(ordering).order('last_name ASC, first_name ASC')
    else
      @users = User.order('section ASC, last_name ASC, first_name ASC')
    end

    @users = @users.reject &:admin?
          respond_to do |format|
        format.js
        format.html
      end
  end

  def show
    @assignments = Assignment.order 'number DESC'
    @submits = @user.submit_counts
    @scores = @user.submit_scores
  end

  def home

    flash.keep
    redirect_to current_user.nil? ? '/users/sign_in' : ( current_user.admin? ? users_path : user_path(current_user) )
    #redirect_to current_user.admin? ? users_path : user_path(current_user);

  end

  private

  def access_check

    if params[:id] != current_user.id.to_s && !current_user.admin? 
      flash[:warning] = 'Permission denied'
      redirect_to root_path and return
    end
    @user = User.find(params[:id]) if params[:id]
    @viewer = current_user
  end
end
