require 'tempfile'
class ProblemsController < ApplicationController
  before_filter :access_check
  Dashes = '-'*80
  RagDir = "#{Rails.root}/rag"
  ScoreRegex = /Score out of (\d+): (\d+)$/
  FeedbackRegex = /#{Dashes}(.*)#{Dashes}/m

  def index
   @submissions = @problem.submissions.where(:user_id=>@user).paginate(:page=>params[:page], :per_page=> 3).order('created_at DESC')
   respond_to do |format|
    format.html
    format.js
  end                                           
end



def submit
  if @user != @viewer
    flash[:warning] = 'Permission denied'
    redirect_to user_path(@user) and return
  end

  if !params[:file] 
    flash[:warning] = 'No file chosen for submission.'
    redirect_to root_path and return
  end

  #if @problem.assignment.number == 2 && 

    submission = Submission.new 
    submission.user = @user
    submission.problem = @problem

    if @deduct.starts_at <= Time.zone.now
      submission.ded = @deduct.penalty
    else
      submission.ded=0
    end

    tempfile = params[:file]

    case @problem.assignment.number
    when 0
      begin
        submission.file = tempfile
        Rails.logger.error "#{submission.file.path}"
        submission.original_filename = tempfile.original_filename
        submission.save
        spec =  "#{spec_dir 0}/part#{@problem.number}_spec.rb"
        result = %x(cd #{RagDir} && ./grade0 #{submission.file.path} #{spec})
        submission.user_raw_score = result.match(ScoreRegex)[2] if result
        submission.score = (result.match(ScoreRegex)[2].to_i * ded_percent(submission)).ceil if result
        submission.feedback = result.match(FeedbackRegex)[1]
        submission.save
        flash[:notice] = "Submission uploaded successfully"
      rescue => ex
        Rails.logger.error "Error (problem#update): #{ex}"
        flash[:warning] = "Submission failed"
      end 
    when 1
      begin
        submission.file = tempfile
        submission.original_filename = tempfile.original_filename
        submission.save
        spec = "#{spec_dir 1}/part#{@problem.number}_spec.rb"
        result = %x(cd #{RagDir} && ./grade #{tempfile.path} #{spec})
        submission.user_raw_score = result.match(ScoreRegex)[2] if result
        submission.score = (result.match(ScoreRegex)[2].to_i * ded_percent(submission)).ceil if result
        submission.feedback = result.match(FeedbackRegex)[1]
        submission.save
        flash[:notice] = "Submission uploaded successfully"
      rescue => ex
        Rails.logger.error "Error (problem#update): #{ex}"
        flash[:warning] = "Submission failed"
      end
    when 2
      begin
        spec = "#{spec_dir 2}/part#{@problem.number}_spec.rb"
        uri = "#{tempfile.read.strip}"
        submission.file = tempfile
        submission.original_filename = tempfile.original_filename
        submission.save
      #uploaded_file = Tempfile.new('uploaded_file')
      #FileUtils.cp tempfile, uploaded_file
        #this is a hack to ensure the users submitted uri is
        #inserted in the spec because the global variables 
        #A. didnt work 
        #B. didnt seem thread safe if they did
        fu = File.open("#{spec_dir 2}/d/#{current_user.id}.rb", 'w+'){
          |f| f.write "#!/usr/bin/env ruby\nuri = "
          #changed from tempfile.path 3.13.13
          f << "\""<<File.open("#{tempfile.path}").read << "\""
          f << "\n"
          f << File.open("#{spec}").read;
          f.close;
        }
        pid = Process::fork do
          result = %x(cd #{RagDir} && ./grade_heroku #{submission.file.path} #{spec_dir 2}/d/#{current_user.id}.rb)
          ActiveRecord::Base.establish_connection
          submission.user_raw_score = result.match(ScoreRegex)[2] if result
          submission.score = (result.match(ScoreRegex)[2].to_i * ded_percent(submission)).ceil if result
          submission.feedback = result.match(FeedbackRegex)[1] if result
          submission.save 
        end
        Process.detach pid
        flash[:notice] = "Submission uploaded successfully"
      rescue => ex
        Rails.logger.error "Error (problem#update): #{ex}"
        flash[:warning] = "Submission failed"
      end
    when 3
      begin
        submission.file = tempfile
        submission.original_filename = tempfile.original_filename
        submission.save
        pid = Process::fork do
          begin
            result = %x(cd #{RagDir} && ./grade3 -a ../hw3_solution #{submission.file.path} hw3.yml)
            ActiveRecord::Base.establish_connection
            submission.user_raw_score = result.match(ScoreRegex)[2] if result
            submission.score = (result.match(ScoreRegex)[2].to_i * ded_percent(submission)).ceil if result
            submission.feedback = result.match(FeedbackRegex)[0]
            submission.save
          rescue => ex
            Rails.logger.error "Error (grade3): #{ex}"
            submission.score = 0
            submission.feedback = "Error (grade3): #{ex}"
            submission.save
          end
        end
        Process.detach pid
        flash[:notice] = "Submission uploaded successfully. See results by refreshing page (It may take several minutes)."
      rescue => ex
        Rails.logger.error "Error (problem#update): #{ex}"
        flash[:warning] = "Submission failed"
      end
    when 4
      begin
        #Need to fix hw4 because when i run te scripts from my terminal
        #the output is correct, however when run from the app the spec
        #in hw4.yaml fails
        submission.file = tempfile
        submission.original_filename = tempfile.original_filename
        submission.save
        pid = Process::fork do
          begin
            result = %x(cd #{RagDir} && ./grade4 #{submission.file.path} hw4.yml)
            ActiveRecord::Base.establish_connection
            submission.user_raw_score = result.match(ScoreRegex)[2] if result
            submission.score = (result.match(ScoreRegex)[2].to_i * ded_percent(submission)).ceil if result
            submission.feedback = result.match(FeedbackRegex)[1]
            submission.save
          rescue => ex
            Rails.logger.error "Error (grade4): #{ex}"
            submission.score = 0
            submission.feedback = 'There was an error processing your submission.  Ensure your submission is in .tar.gz format.'
            submission.save
          end
        end
        Process.detach pid
        flash[:notice] = "Submission uploaded successfully. See results by refreshing page (It may take several minutes)."
      rescue => ex    
        Rails.logger.error "Error (problem#update): #{ex}"
        flash[:warning] = "Submission failed"
      end
    end
    redirect_to submissions_path @user, @problem 
  end

  private

  def ded_percent s
    (1-(s.ded.to_i * 0.01))
  end

  def spec_dir n
    "#{Rails.root}/saasbook-cs169-s12-hw-solutions/hw#{n}/spec"
  end

  def access_check
    @user = User.find(params[:user_id])
    @viewer = current_user
    if @user.id != @viewer.id && !@viewer.admin?
      flash[:warning] = 'Permission denied'
      redirect_to user_path(@user) and return
    end
    @problem = Problem.find(params[:id])
    @deduct=Deduction.find_by_assignment_id(@problem.assignment)
  end

end
