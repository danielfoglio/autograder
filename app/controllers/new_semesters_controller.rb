require 'rake'
require 'csv'
require 'tempfile'
class NewSemestersController < ApplicationController
  before_filter :admin_check, :check_pass
  def delete_db_data year=Time.new.year, month=Time.new.month
  	begin
     %x[cd #{Rails.root} && sqlite3 #{current_db} '.backup #{hold_db}.sqlite3']
     %x[cd #{Rails.root} && rake db:drop db:create db:migrate] 
     %x[cd #{Rails.root} && rake data:add_ilson_and_admin3155]
	  rescue => ex
     Rails.logger.error "Error (new_semesters_controller#delete_db_data): #{ex}"
     flash[:warning] = "Error Destroying Data In Database #{ex}" 
    end
    if !User.find_by_username 'Admin3155'
    	User.create!(:admin=>true, :password=>'!itcs!3155!', :first_name => 'Admin', :last_name => 'Admin', :password_confirmation=>'!itcs!3155!', :username=>'admin3155', :email=>'itcsautograder@gmail.com', :section=>0, :student_id=>0)
    end
     #You are now logged in as #{current_user} and your password is  '  !itcs!3155!  '"
     if User.all.size == 2
     	flash[:warning] = "Data Successfully Destroyed, Sign in to continue."
        redirect_to semester_path
    else
    	redirect_to root_path
    end
  end

  def import_new_roster
     if !params[:file].present?
      flash[:filr] = 'Error: No file chosen for submission.'
      redirect_to semester_path and return
     elsif params[:file].original_filename !~ /.*\.csv$/i
      flash[:filr] = 'Error: File must be in CSV format. Ensure it has .csv ending!'
      redirect_to semester_path and return     	
     end
     tempfile=params[:file]
     file="#{Rails.root}/lib/tasks/rosters/#{g_time.year}-#{g_time.month}.csv"
     begin
     %x[cd #{Rails.root} && cp #{tempfile.tempfile.path} #{file} && chmod 777 #{file}]
     #sign_in(User.find(2)) :bypass => true
    #end
    #Process::detach pid
	rescue => ex
     Rails.logger.error "Error (new_semesters_controller#import_new_roster): #{ex}"
     flash[:filr] = "Error Uploading File #{ex}" 
    end
     flash[:warning] = "New Roster Successfully Imported."#You are now logged in as #{current_user} and your password is  '  !itcs!3155!  '"
     redirect_to semester_path
  end

  def csv_example
      send_file  "/#{Rails.root}/lib/tasks/rosters/0.csv", filename: 'example.csv'
  end

  def reseed_db
  	begin
  	  %x[cd #{Rails.root} && rake data:update_from_roster --trace]
    rescue => ex
     Rails.logger.error "Error (new_semesters_controller#reseed_db): #{ex}"
     flash[:warning] = "Error Updating Roster. The Problem May Stem From A Bad CSV File So Be Sure It Looks Like The Example When Opened In vim: #{ex}"
     end
     flash[:warning] = "You Should Be All Set For Next Semester!!! Make Sure Everything In The User List Is Correct, If Not: The Problem 99% Of The Time Will Be In The CSV."
     redirect_to assignments_path
  end

  private
  def g_time
  	Time.new
  end
  def hold_db year=Time.new.year, month=Time.new.month
  	"#{Rails.root}/db/pastsemesters/#{month}-#{year}#{Rails.env}"
  end

  def current_db
	"#{Rails.root}/db/#{Rails.env}.sqlite3"
  end
end