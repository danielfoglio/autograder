class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :no_cache
  before_filter :authenticate_user!, :except => [:home]


  def admin_check
	 if !current_user.admin?
      	flash[:warning] = 'Permission denied'
      	redirect_to root_path and return
    end
  end

  def display_date
    self.strftime("%d %B %Y at %I:%M%p") unless !self
  end
  def check_pass
      if current_user && !current_user.password_change
        flash[:alert]= "YOU MUST CHANGE YOUR PASSWORD BEFORE YOU CAN CONTINUE\n"
        redirect_to user_edit_path current_user
      end
    nil
  end
  #below is to use devise in any controller, i think??
  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  private

  def no_cache
    expires_in 0
  end
end