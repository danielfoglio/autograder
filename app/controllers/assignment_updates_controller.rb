class AssignmentUpdatesController < ApplicationController
  before_filter :access_check, :check_pass
  
  def index
    @assignments = Assignment.all
  end
  
  def edit
  	@assignment = Assignment.find(params[:id])
  	@deduction = @assignment.get_deduction
  end

  def update
    if params[:commit] == 'Cancel'
      redirect_to assignments_path and return
    else
	    @assignment = Assignment.find(params[:id])
	    @deduction = Deduction.find_or_initialize_by_assignment_id(params[:id])
		  @deduction.penalty = params[:assignment][:deduction][:penalty]
      h = params[:assignment][:deduction].delete_if{|key, value| key == "penalty"}
      h=h.values.join(',').gsub(',','-') <<" 00:05:03"
		  @deduction.starts_at = DateTime.parse(h)
		  @deduction.save
    end
    if @assignment.update_attributes(params[:assignment].except(:deduction))
      redirect_to assignments_path, notice: 'Assignment was successfully updated.' 
    else
      render action: "edit", warning: "Error in Assignment Update"  
    end
  end

  def add
    a= Announcement.new
      a.message = params[:message]
      a.starts_at = Time.zone.now
      s=params[:date].values.join(',').gsub(',','-') << " 00:04:59"
      a.ends_at = s
      a.save!    
      redirect_to assignments_path
  end

  def copy_2_staging
    %x(sqlite3 #{Rails.root}/db/production.sqlite3 '.backup #{Rails.root}/db/staging.sqlite3')
  end

  def restore_from_staging
    %x(sqlite3 #{Rails.root}/db/production.sqlite3 '.restore #{Rails.root}/db/staging.sqlite3')
  end

  def hw0
    if(!Assignment.find_by_number 0)
      a0 = Assignment.create! :number=>0, :description=>"Ruby Warm-up"
      1.upto(3).each do |n|
        problem = Problem.find_or_initialize_by_number_and_assignment_id(n, a0.id)
        problem.max_score = 100
        problem.save!
      end
    end 
      respond_to do |format|
        format.html {redirect_to assignments_path}
        format.js 
      end
  end

  def hw1
    if(!Assignment.find_by_number 1) 
      a1 = Assignment.create! :number=>1, :description=>"Ruby Calisthenics"
      1.upto(6).each do |n|
        problem = Problem.find_or_initialize_by_number_and_assignment_id(n, a1.id)
        problem.max_score = 100
        problem.save!
      end
      #flash[:notice]='Successfully added hw1!'
    end 
      respond_to do |format|
        format.html {redirect_to assignments_path}
        format.js 
      end
  end

  def hw2
    if(!Assignment.find_by_number 2)
      a2 = Assignment.find_or_initialize_by_number 2
      a2.description = 'Intro to Rails'
      a2.save!
      1.upto(3).each do |n|
        problem = Problem.find_or_initialize_by_number_and_assignment_id(n, a2.id)
        problem.max_score = 100
        problem.save!
      end
    end 
      respond_to do |format|
        format.html {redirect_to assignments_path}
        format.js 
      end
  end

  def hw3
    if(!Assignment.find_by_number 3)
      a3 = Assignment.find_or_initialize_by_number 3
      a3.description = 'BDD & Cucumber'
      a3.save!
      problem = Problem.find_or_initialize_by_number_and_assignment_id(1, a3.id)
      problem.max_score = 500
      problem.save!
    end 
      respond_to do |format|
        format.html {redirect_to assignments_path}
        format.js 
      end
  end

  def hw4
    if(!Assignment.find_by_number 4)
      a4 = Assignment.find_or_initialize_by_number 4
      a4.description = 'BDD and TDD Cycle'
      a4.save!
      problem = Problem.find_or_initialize_by_number_and_assignment_id(1, a4.id)
      problem.max_score = 500
      problem.save!
    end 
      respond_to do |format|
        format.html {redirect_to assignments_path}
        format.js 
      end
  end

  private

  def access_check
    if params[:id] != current_user.id.to_s && !current_user.admin? 
      flash[:warning] = 'Permission denied'
      redirect_to root_path and return
    end
  end

end