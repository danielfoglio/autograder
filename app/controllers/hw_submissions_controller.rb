class HwSubmissionsController < ApplicationController
  def hw2source
    if !params[:hw2source] 
      flash[:warning] = 'No file chosen for submission.'
      redirect_to root_path and return
    end

    if (newhw2 = HwSubmission.find_by_user_id params[:user_id])
      newhw2.hw2source = params[:hw2source]
    else
      newhw2 = HwSubmission.new
      newhw2.user_id = params[:user_id]
      newhw2.hw2source = params[:hw2source]
    end
    if newhw2.save
      flash[:notice] = "Homework 2 source successfully uploaded"
    else
      flash[:warning]= "Couldn't add Homework 2 source"
    end
    redirect_to root_path
  end

  def ajax_file_dl
    if (newhw2 = HwSubmission.find_by_user_id params[:id])
      send_file "/#{newhw2.hw2source.path}", {:type => "#{newhw2.hw2source_content_type}", :filename => "#{newhw2.hw2source_file_name}"}
    end
  end
end
