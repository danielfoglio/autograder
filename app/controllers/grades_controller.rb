require 'csv'

class GradesController < ApplicationController
  before_filter :access_check, :check_pass

  def summary
    @header = summary_header
    @data = summary_data
    respond_to do |format|
      format.html
      format.csv { send_data to_csv(@header, @data),
          :filename=>filename('summary'),
          :type=>'text/csv; charset=utf-8; header=present' }
    end
  end

  def full
    @header = full_header
    @data = full_data
    respond_to do |format|
      format.html
      format.csv { send_data to_csv(@header, @data),
          :filename=>filename('full'),
          :type=>'text/csv; charset=utf-8; header=present' }
    end
  end

  private
  
  def filename base
    "#{base}-#{Time.now.strftime '%Y-%m-%d-%H%M%S'}.csv"
  end

  def summary_header
    header = ['Last Name','First Name','Student ID#','NinerNet ID','Section']
    Assignment.order(:number).each do |a|
      a.problems.order(:number).each do |p|
        header << "a#{a.number}p#{p.number} score"
      end
      header << "a#{a.number} total"
    end
    header
  end

  def summary_data
    Class.new do
      include Enumerable
      def each
        User.where(:admin=>false).each do |u|
          scores = u.submit_scores
          row = [u.last_name, u.first_name, u.student_id, u.username, u.section]
          Assignment.order(:number).each do |a|
            total = 0
            a.problems.order(:number).each do |p|
              score = scores[p] || 0
              total += score
              row << score
            end
            row << total
          end
          yield row
        end
      end
    end.new
  end

  def full_header
    ['Last Name','First Name','Student ID#','NinerNet ID','Section','Submission Time',
     'Assignment#','Problem#','Raw Score','Final Score']
  end

  def full_data
    Class.new do
      include Enumerable
      def each
        Submission.find_each do |s|
          user = s.user
          problem = s.problem
          assignment = problem.assignment
          yield [user.last_name, user.first_name, user.student_id, user.username,
            user.section, GradesController::local_time(s.created_at.to_time, false), 
            assignment.number, problem.number, s.score, s.final_score]
        end
      end
    end.new
  end

  def to_csv(header, data, options = {})
    CSV.generate(options) do |csv|
      csv << header
      data.each do |row|
        csv << row
      end
    end
  end

  def access_check
    if !current_user.admin?
      flash[:warning] = 'Permission denied'
      redirect_to root_path and return
    end
  end

  def self.local_time time, zone = true
    #eastern = ActiveSupport::TimeZone["Eastern Time (US & Canada)"]
    #eastern.at(time).strftime "%a, %b %e, %l:%M:%S %p#{' %Z' if zone}"
    time.strftime "%a, %b %e, %l:%M:%S %p#{' %Z' if zone}"

  end

end
