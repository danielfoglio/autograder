module AssignmentsHelper
  def total_submissions user, assignment
    Submission.joins(:user, {:problem=> :assignment}).
      where(:problems=>{:assignment_id=>assignment},:user_id=>user).count
  end

  def total_score user, assignment
    total = 0
    user.submit_scores.each do |problem, score|
      total += score if score && problem.assignment == assignment
    end
    total
  end
end
