module ApplicationHelper

  def local_time time, zone = true
    eastern = ActiveSupport::TimeZone["Eastern Time (US & Canada)"]
    eastern.at(time).strftime "%a, %b %e, %l:%M:%S %p#{' %Z' if zone}"
  end
end
