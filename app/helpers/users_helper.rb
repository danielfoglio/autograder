module UsersHelper
  def last_submission_time(user)
    if user.last_submission == User::Never 
      'N/A'
    else
      local_time(user.last_submission)
    end
  end

  def column_header(col)
    if col[:sort]
      link_to col[:title], users_path(sort: col[:number], dir: col[:dir]) 
    else
      col[:title]
    end
  end
end
