module SubmissionsHelper
  def score_report submission, final = true, colorize = true
    score = final ? submission.score : submission.user_raw_score unless submission.nil?
    if score.nil? || score < 0
      score = 0
      submission.score = 0
    end

    if colorize
      case score
      when 0
        color = 'red'
      when 1..99
        color = 'orange'
      else
        color = 'green'
      end
      score = "<span style=\"color:#{color};\">#{score}</span>"
    end
    "#{score} / #{submission.problem.max_score}"
  end
end
