class Assignment < ActiveRecord::Base
  attr_accessible :description, :due_time, :number, :open_time, :deduction

  def max_score 
    problems.sum :max_score
  end
  
  has_many :problems
  has_many :submissions, :through => :problems
  has_many :deductions
def get_deduction
	Deduction.find_by_assignment_id(self.id) ? Deduction.find_by_assignment_id(self.id).penalty : 0
end
end
