class Deduction < ActiveRecord::Base
  attr_accessible :penalty, :starts_at, :assignment_id
  belongs_to :assignment
end
