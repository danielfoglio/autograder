class Problem < ActiveRecord::Base
  attr_accessible :max_score

  def score user
    submissions.where(:user=>user).maximum(:score) || 0
  end

  belongs_to :assignment
  has_many :submissions

end
