class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable,
         :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
   Never = Time.new(0)
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  #:valadates :password_confirmation, :presence => true

  # Setup accessible (or protected) attributes for your model
  attr_accessible :login, :username, :email, :current_password, :password, :first_name, :last_name, :student_id, :section,
    :password_confirmation, :remember_me, :admin
  attr_accessor :login

  has_many :submissions

  def submit_scores
    result = {}
    Problem.find_each do |p|
      result[p] = submissions.where(:problem_id => p.id).map(&:final_score).reject{|n| !n}.max
    end
    result #this may be where bad results are coming from
  end

  def submit_counts
    submissions.count :group => :problem_id
  end

  def full_name(current_user)
    "#{'Admin ' if current_user.admin?}#{current_user.first_name} #{current_user.last_name}"
  end

  def last_submission
    last = submissions.order(:created_at).last
    last ? last.created_at.to_time : Never
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
end
