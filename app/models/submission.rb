require 'paperclip'

class Submission < ActiveRecord::Base
  attr_accessible :done, :feedback, :score, :file, :hw2source
  belongs_to :problem
  belongs_to :user

  has_attached_file :file#,:path => "#{Rails.root}/public/system/submissions/s13/#{self.user.id/self.problem.assignment.id}/"
  #,
   # :storage => :s3,
    #:s3_credentials => {
     # :bucket            => "#{ENV['S3_BUCKET_NAME']}#{'-dev' if Rails.env.development?}",
      #:access_key_id     => ENV['AWS_ACCESS_KEY_ID'],
      #:secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
    #}

  def deduction
   # assignment = problem.assignment
    #deductions = assignment.deductions.where(:starts_at=>assignment.due_time..self.created_at)
    #deductions.order('starts_at DESC').limit(1).first
    nil
  end

  def final_score
    #(100 - self.deduction.penalty)               changed to self.score from just score
    self.deduction ? (score * (100 - self.deduction) / 100.0).round : self.score
  end

end
