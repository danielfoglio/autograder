class Movie < ActiveRecord::Base

  scope :rated, lambda {|r| where(:rating => r) }

  def self.all_ratings
    %w(G PG PG-13 NC-17 R)
  end
end
