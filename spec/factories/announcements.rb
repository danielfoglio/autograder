# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :announcement do
    message "MyText"
    starts_at "2013-01-27 10:31:19"
    ends_at "2013-01-28 10:31:19"
  end
end
