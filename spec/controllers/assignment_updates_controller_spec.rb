require 'spec_helper'
include Devise::TestHelpers
describe AssignmentUpdatesController do

  describe "GET 'index'" do
    it "should be successful" do
      visit assignments_path
      	signin
      	fill_in "Login", :with => 'dfoglio'
		fill_in "Password", :with => '800768163'
		click_button "Sign in"
      response.should be_success
    end
  end

end
