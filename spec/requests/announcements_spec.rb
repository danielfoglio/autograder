require 'spec_helper'
include Devise::TestHelpers
describe "Announcements" do
	
    it "displays active announcements" do
    	Announcement.create! message: "Testing TT", starts_at: 1.hour.ago, ends_at: 1.hour.from_now
    	Announcement.create! message: "Upcoming", starts_at: 1.hour.from_now, ends_at: 1.day.from_now
    	visit root_path 
    	#@a=User.create! :username=>'pachary2', :email=>'pachary2@uncc.edu', :password=>'800676756', :password_confirmation=>'800676756'
      #@a.update_attribute :admin, true
      #sign_in @a
    	page.should have_content("Testing TT")
    	page.should_not have_content("Upcoming")
    	#click_on "Hide"
  		#page.should_not have_content("Testing TT")
  end

      it "stays on page when hiding announcement with js", js: true do
    	Announcement.create! message: "Testing TT", starts_at: 1.hour.ago, ends_at: 1.hour.from_now
    	visit root_path
      #@a=User.create! :username=>'pachary2', :email=>'pachary2@uncc.edu', :password=>'800676756', :password_confirmation=>'800676756'
    	#@a.update_attribute :admin, true
      #sign_in @a
    	page.should have_content("Testing TT")
    	#expect{ click_on "Hide" }.to_not change { page.response_headers }
  		#page.should_not have_content("Testing TT")
  end
end
