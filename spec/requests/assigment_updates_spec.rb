require 'spec_helper'
include Devise::TestHelpers
describe "AssigmentUpdates" do
	it 'should display all current assignments' do
		Assignment.create! number: '1', description: 'hw1'
		Assignment.create! number: '2', description: 'hw2'
		Assignment.create! number: '3', description: 'hw3'
		Assignment.create! number: '4', description: 'hw4'
		visit root_path
	signin
		fill_in "Login", :with => 'dfoglio'
		fill_in "Password", :with => '800768163'
		click_button "Sign in"
		visit assignments_path
		page.should have_content("Assignments")
		1.upto(4).each do |n|
			page.should have_content("hw#{n}")
		end
	end


  #describe "GET /assignment_updates" do
    #it "works! (now write some real specs)" do
      #get assignment_updates_path
    
    it 'should allow you to update an assignments due date' do
    end

end
