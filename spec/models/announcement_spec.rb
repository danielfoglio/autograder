require 'spec_helper'
include Devise::TestHelpers
describe Announcement do
  
  it "has current scope" do
  	passed = Announcement.create! starts_at: 1.day.ago, ends_at: 1.hour.ago
  	current = Announcement.create! starts_at: 1.hour.ago, ends_at: 1.hour.from_now
  	future = Announcement.create! starts_at: 1.hour.from_now, ends_at: 1.day.from_now
  	Announcement.current.should eq([current])

end
  it " doesnt have ids passed in current" do
  	current1 = Announcement.create! starts_at: 1.day.ago, ends_at: 1.day.from_now
  	  	current2 = Announcement.create! starts_at: 1.day.ago, ends_at: 1.day.from_now
  	  	Announcement.current([current2.id]).should eq([current1])
  	  end

  it "includes current when passed nil" do
  	current = Announcement.create! starts_at: 1.hour.ago, ends_at: 1.hour.from_now
  	Announcement.current(nil).should eq([current])
  end

end
