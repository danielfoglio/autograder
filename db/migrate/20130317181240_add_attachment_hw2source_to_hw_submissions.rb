class AddAttachmentHw2sourceToHwSubmissions < ActiveRecord::Migration
  def self.up
    change_table :hw_submissions do |t|
      t.attachment :hw2source
    end
  end

  def self.down
    drop_attached_file :hw_submissions, :hw2source
  end
end
