class AddDeductionsToProblems < ActiveRecord::Migration
  def up
  	remove_column :deductions, :penalty
    add_column :deductions, :penalty, :integer, :default => '0'
  end
end
