class AddFileToSubmissions < ActiveRecord::Migration
  def up
    add_attachment :submissions, :file
  end
        
  def down
    remove_attachment :submissions, :file
  end
end
