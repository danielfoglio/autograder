class CreateSubmissions < ActiveRecord::Migration
  def change
    create_table :submissions do |t|
      t.integer :score
      t.text :feedback
      t.boolean :done
      t.belongs_to :problem
      t.timestamps
    end
  end
end
