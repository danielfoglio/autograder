class AddUsersToSubmissions < ActiveRecord::Migration
  def up
    remove_column :submissions, :user
    add_column :submissions, :user_id, :integer
  end

  def down
    remove_column :submissions, :user_id
    add_column :submissions, :user, :string
  end
end
