class AddPassChangeRedirectUntilTheyDooDoo < ActiveRecord::Migration
  def up
  	add_column :users, :password_change, :boolean, default: false
  end

  def down
  	drop_column :users, :password_change, :boolean
  end
end
