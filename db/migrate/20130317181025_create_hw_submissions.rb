class CreateHwSubmissions < ActiveRecord::Migration
  def change
    create_table :hw_submissions do |t|
      t.integer :user_id
      t.timestamps
    end
  end
end
