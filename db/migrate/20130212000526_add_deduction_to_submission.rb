class AddDeductionToSubmission < ActiveRecord::Migration
  def up
  	add_column :submissions, :ded, :integer, default: 0
  end

  def down
  	drop_column :submissions, :ded, :integer
  end
end

