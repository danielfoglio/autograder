class CreateAssignmentUpdates < ActiveRecord::Migration
  def change
    create_table :assignment_updates do |t|

      t.timestamps
    end
  end
end
