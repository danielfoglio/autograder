class AddFieldsToUsers < ActiveRecord::Migration
  def up
    add_column :users, :student_id, :string
    add_column :users, :last_name, :string
    add_column :users, :first_name, :string
    add_column :users, :middle_init, :string
    add_column :users, :section, :integer
  end

  def down
    remove_column :users, :student_id
    remove_column :users, :last_name
    remove_column :users, :first_name
    remove_column :users, :middle_init
    remove_column :users, :section
  end
end
