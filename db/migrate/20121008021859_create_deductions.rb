class CreateDeductions < ActiveRecord::Migration
  def change
    create_table :deductions do |t|
      t.integer :penalty # this is the percent penalty
      t.datetime :starts_at
      t.references :assignment
      t.timestamps
    end
  end
end
