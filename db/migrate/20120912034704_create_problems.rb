class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.integer :max_score
      t.belongs_to :assignment
      t.timestamps
    end
  end
end
