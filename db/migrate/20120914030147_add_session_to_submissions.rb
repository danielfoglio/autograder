class AddSessionToSubmissions < ActiveRecord::Migration
  def change
    add_column :submissions, :session, :text
  end
end
