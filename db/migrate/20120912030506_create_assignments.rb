class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.integer :number
      t.text :description
      t.datetime :open_time
      t.datetime :due_time
      t.timestamps
    end
  end
end
