class RemoveDoneFromSubmissions < ActiveRecord::Migration
  def up
    add_column :submissions, :user, :string
    remove_column :submissions, :done
    remove_column :submissions, :session
  end

  def down
    remove_column :submissions, :user
    add_column :submissions, :done
    add_column :submissions, :session
  end
end
