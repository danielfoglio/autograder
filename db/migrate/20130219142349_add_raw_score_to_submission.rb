class AddRawScoreToSubmission < ActiveRecord::Migration
  def up
  	add_column :submissions, :user_raw_score, :integer, default: 0
  end

  def down
  	drop_column :submissions, :user_raw_score, :integer
  end
end
