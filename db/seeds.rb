# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
a0 = Assignment.create! :number=>0, :description=>"Ruby Warm-up"
1.upto(3).each do |n|
      problem = Problem.find_or_initialize_by_number_and_assignment_id(n, 1)
      problem.max_score = 100
      problem.save!
    end


users = [

{:password=>'!UnCc1679$',:password_confirmation=>'!UnCc1679$', :username => 'halasiri',:email =>'halasiri@uncc.edu'},

{:password=>'!UnCc6244$',:password_confirmation=>'!UnCc6244$', :username => 'mbattag4',:email =>'mbattag4@uncc.edu'},

{:password=>'!UnCc8736$',:password_confirmation=>'!UnCc8736$', :username => 'cbayne1',:email =>'cbayne1@uncc.edu'},

{:password=>'!UnCc4861$',:password_confirmation=>'!UnCc4861$', :username => 'fbecker',:email =>'fbecker@uncc.edu'},

{:password=>'!UnCc0911$',:password_confirmation=>'!UnCc0911$', :username => 'kblack35',:email =>'kblack35@uncc.edu'},

{:password=>'!UnCc0067$',:password_confirmation=>'!UnCc0067$', :username => 'gbowes',:email =>'gbowes@uncc.edu'},

{:password=>'!UnCc9266$',:password_confirmation=>'!UnCc9266$', :username => 'mbrow237',:email =>'mbrow237@uncc.edu'},

{:password=>'!UnCc0526$',:password_confirmation=>'!UnCc0526$', :username => 'bbystrom',:email =>'bbystrom@uncc.edu'},

{:password=>'!UnCc3238$',:password_confirmation=>'!UnCc3238$', :username => 'ecamarat',:email =>'ecamarat@uncc.edu'},

{:password=>'!UnCc3518$',:password_confirmation=>'!UnCc3518$', :username => 'bcarte36',:email =>'bcarte36@uncc.edu'},

{:password=>'!UnCc8035$',:password_confirmation=>'!UnCc8035$', :username => 'ccarviou',:email =>'ccarviou@uncc.edu'},

{:password=>'!UnCc0997$',:password_confirmation=>'!UnCc0997$', :username => 'tjcaughm',:email =>'tjcaughm@uncc.edu'},

{:password=>'!UnCc5659$',:password_confirmation=>'!UnCc5659$', :username => 'bchris19',:email =>'bchris19@uncc.edu'},

{:password=>'!UnCc5513$',:password_confirmation=>'!UnCc5513$', :username => 'acostin',:email =>'acostin@uncc.edu'},

{:password=>'!UnCc3379$',:password_confirmation=>'!UnCc3379$', :username => 'tdetlef',:email =>'tdetlef@uncc.edu'},

{:password=>'!UnCc6055$',:password_confirmation=>'!UnCc6055$', :username => 'melouria',:email =>'melouria@uncc.edu'},

{:password=>'!UnCc6225$',:password_confirmation=>'!UnCc6225$', :username => 'jflemi27',:email =>'jflemi27@uncc.edu'},

{:password=>'!UnCc5286$',:password_confirmation=>'!UnCc5286$', :username => 'wflower3',:email =>'wflower3@uncc.edu'},

{:password=>'!UnCc6297$',:password_confirmation=>'!UnCc6297$', :username => 'nfrank1',:email =>'nfrank1@uncc.edu'},

{:password=>'!UnCc9474$',:password_confirmation=>'!UnCc9474$', :username => 'cgibso33',:email =>'cgibso33@uncc.edu'},

{:password=>'!UnCc6772$',:password_confirmation=>'!UnCc6772$', :username => 'cgrego19',:email =>'cgrego19@uncc.edu'},

{:password=>'!UnCc0936$',:password_confirmation=>'!UnCc0936$', :username => 'ahall77',:email =>'ahall77@uncc.edu'},

{:password=>'!UnCc3683$',:password_confirmation=>'!UnCc3683$', :username => 'rharbins',:email =>'rharbins@uncc.edu'},

{:password=>'!UnCc2245$',:password_confirmation=>'!UnCc2245$', :username => 'chenley3',:email =>'chenley3@uncc.edu'},

{:password=>'!UnCc6632$',:password_confirmation=>'!UnCc6632$', :username => 'jhenriqu',:email =>'jhenriqu@uncc.edu'},

{:password=>'!UnCc6871$',:password_confirmation=>'!UnCc6871$', :username => 'chickey2',:email =>'chickey2@uncc.edu'},

{:password=>'!UnCc7118$',:password_confirmation=>'!UnCc7118$', :username => 'wholguin',:email =>'wholguin@uncc.edu'},

{:password=>'!UnCc2267$',:password_confirmation=>'!UnCc2267$', :username => 'jkelle26',:email =>'jkelle26@uncc.edu'},

{:password=>'!UnCc3394$',:password_confirmation=>'!UnCc3394$', :username => 'ckettler',:email =>'ckettler@uncc.edu'},

{:password=>'!UnCc1422$',:password_confirmation=>'!UnCc1422$', :username => 'clutz10',:email =>'clutz10@uncc.edu'},

{:password=>'!UnCc8368$',:password_confirmation=>'!UnCc8368$', :username => 'cmart138',:email =>'cmart138@uncc.edu'},

{:password=>'!UnCc4574$',:password_confirmation=>'!UnCc4574$', :username => 'nmatin',:email =>'nmatin@uncc.edu'},

{:password=>'!UnCc5629$',:password_confirmation=>'!UnCc5629$', :username => 'jmedd',:email =>'jmedd@uncc.edu'},

{:password=>'!UnCc9459$',:password_confirmation=>'!UnCc9459$', :username => 'amill186',:email =>'amill186@uncc.edu'},

{:password=>'!UnCc1965$',:password_confirmation=>'!UnCc1965$', :username => 'jmills64',:email =>'jmills64@uncc.edu'},

{:password=>'!UnCc3961$',:password_confirmation=>'!UnCc3961$', :username => 'jmujica',:email =>'jmujica@uncc.edu'},

{:password=>'!UnCc0258$',:password_confirmation=>'!UnCc0258$', :username => 'joneill7',:email =>'joneill7@uncc.edu'},

{:password=>'!UnCc5589$',:password_confirmation=>'!UnCc5589$', :username => 'mpatel92',:email =>'mpatel92@uncc.edu'},

{:password=>'!UnCc0231$',:password_confirmation=>'!UnCc0231$', :username => 'lpinerua',:email =>'lpinerua@uncc.edu'},

{:password=>'!UnCc5734$',:password_confirmation=>'!UnCc5734$', :username => 'cradspin',:email =>'cradspin@uncc.edu'},

{:password=>'!UnCc3722$',:password_confirmation=>'!UnCc3722$', :username => 'bsain8',:email =>'bsain8@uncc.edu'},

{:password=>'!UnCc6883$',:password_confirmation=>'!UnCc6883$', :username => 'jsartell',:email =>'jsartell@uncc.edu'},

{:password=>'!UnCc4016$',:password_confirmation=>'!UnCc4016$', :username => 'kshaw29',:email =>'kshaw29@uncc.edu'},

{:password=>'!UnCc0240$',:password_confirmation=>'!UnCc0240$', :username => 'tqshay',:email =>'tqshay@uncc.edu'},

{:password=>'!UnCc8303$',:password_confirmation=>'!UnCc8303$', :username => 'jshuffle',:email =>'jshuffle@uncc.edu'},

{:password=>'!UnCc9459$',:password_confirmation=>'!UnCc9459$', :username => 'jspenc45',:email =>'jspenc45@uncc.edu'},

{:password=>'!UnCc5527$',:password_confirmation=>'!UnCc5527$', :username => 'sstrang',:email =>'sstrang@uncc.edu'},

{:password=>'!UnCc1754$',:password_confirmation=>'!UnCc1754$', :username => 'sstreepe',:email =>'sstreepe@uncc.edu'},

{:password=>'!UnCc8757$',:password_confirmation=>'!UnCc8757$', :username => 'bumbarge',:email =>'bumbarge@uncc.edu'},

{:password=>'!UnCc5696$',:password_confirmation=>'!UnCc5696$', :username => 'dwest20',:email =>'dwest20@uncc.edu'},

{:password=>'!UnCc2080$',:password_confirmation=>'!UnCc2080$', :username => 'mwill297',:email =>'mwill297@uncc.edu'},

{:password=>'!UnCc6863$',:password_confirmation=>'!UnCc6863$', :username => 'fyoung3',:email =>'fyoung3@uncc.edu'},

{:password=>'!UnCc5791$',:password_confirmation=>'!UnCc5791$', :username => 'halshafi',:email =>'halshafi@uncc.edu'},

{:password=>'!UnCc5909$',:password_confirmation=>'!UnCc5909$', :username => 'sanand1',:email =>'sanand1@uncc.edu'},

{:password=>'!UnCc5306$',:password_confirmation=>'!UnCc5306$', :username => 'rarun',:email =>'rarun@uncc.edu'},

{:password=>'!UnCc6185$',:password_confirmation=>'!UnCc6185$', :username => 'fbestowr',:email =>'fbestowr@uncc.edu'},

{:password=>'!UnCc6525$',:password_confirmation=>'!UnCc6525$', :username => 'kbradl16',:email =>'kbradl16@uncc.edu'},

{:password=>'!UnCc5159$',:password_confirmation=>'!UnCc5159$', :username => 'kbrow242',:email =>'kbrow242@uncc.edu'},

{:password=>'!UnCc6268$',:password_confirmation=>'!UnCc6268$', :username => 'nbrown53',:email =>'nbrown53@uncc.edu'},

{:password=>'!UnCc8140$',:password_confirmation=>'!UnCc8140$', :username => 'rburns11',:email =>'rburns11@uncc.edu'},

{:password=>'!UnCc4232$',:password_confirmation=>'!UnCc4232$', :username => 'kchurc13',:email =>'kchurc13@uncc.edu'},

{:password=>'!UnCc9666$',:password_confirmation=>'!UnCc9666$', :username => 'atoscan2',:email =>'atoscan2@uncc.edu'},

{:password=>'!UnCc9952$',:password_confirmation=>'!UnCc9952$', :username => 'jdavi329',:email =>'jdavi329@uncc.edu'},

{:password=>'!UnCc8307$',:password_confirmation=>'!UnCc8307$', :username => 'pdhital',:email =>'pdhital@uncc.edu'},

{:password=>'!UnCc8886$',:password_confirmation=>'!UnCc8886$', :username => 'adyer8',:email =>'adyer8@uncc.edu'},

{:password=>'!UnCc2799$',:password_confirmation=>'!UnCc2799$', :username => 'mesteban',:email =>'mesteban@uncc.edu'},

{:password=>'!UnCc4850$',:password_confirmation=>'!UnCc4850$', :username => 'jfoste56',:email =>'jfoste56@uncc.edu'},

{:password=>'!UnCc1367$',:password_confirmation=>'!UnCc1367$', :username => 'khende39',:email =>'khende39@uncc.edu'},

{:password=>'!UnCc2906$',:password_confirmation=>'!UnCc2906$', :username => 'whillie',:email =>'whillie@uncc.edu'},

{:password=>'!UnCc3766$',:password_confirmation=>'!UnCc3766$', :username => 'kho5',:email =>'kho5@uncc.edu'},

{:password=>'!UnCc6265$',:password_confirmation=>'!UnCc6265$', :username => 'tjohn182',:email =>'tjohn182@uncc.edu'},

{:password=>'!UnCc6586$',:password_confirmation=>'!UnCc6586$', :username => 'ckendal3',:email =>'ckendal3@uncc.edu'},

{:password=>'!UnCc8398$',:password_confirmation=>'!UnCc8398$', :username => 'eking33',:email =>'eking33@uncc.edu'},

{:password=>'!UnCc0108$',:password_confirmation=>'!UnCc0108$', :username => 'rking61',:email =>'rking61@uncc.edu'},

{:password=>'!UnCc4093$',:password_confirmation=>'!UnCc4093$', :username => 'tlaney6',:email =>'tlaney6@uncc.edu'},

{:password=>'!UnCc6108$',:password_confirmation=>'!UnCc6108$', :username => 'slinnen',:email =>'slinnen@uncc.edu'},

{:password=>'!UnCc9288$',:password_confirmation=>'!UnCc9288$', :username => 'lxinghe',:email =>'lxinghe@uncc.edu'},

{:password=>'!UnCc7494$',:password_confirmation=>'!UnCc7494$', :username => 'pmille41',:email =>'pmille41@uncc.edu'},

{:password=>'!UnCc0782$',:password_confirmation=>'!UnCc0782$', :username => 'jminter3',:email =>'jminter3@uncc.edu'},

{:password=>'!UnCc3614$',:password_confirmation=>'!UnCc3614$', :username => 'cmosele2',:email =>'cmosele2@uncc.edu'},

{:password=>'!UnCc4159$',:password_confirmation=>'!UnCc4159$', :username => 'bocampo',:email =>'bocampo@uncc.edu'},

{:password=>'!UnCc5818$',:password_confirmation=>'!UnCc5818$', :username => 'jpage27',:email =>'jpage27@uncc.edu'},

{:password=>'!UnCc7328$',:password_confirmation=>'!UnCc7328$', :username => 'rfqueena',:email =>'rfqueena@uncc.edu'},

{:password=>'!UnCc3733$',:password_confirmation=>'!UnCc3733$', :username => 'jramire7',:email =>'jramire7@uncc.edu'},

{:password=>'!UnCc4479$',:password_confirmation=>'!UnCc4479$', :username => 'priley12',:email =>'priley12@uncc.edu'},

{:password=>'!UnCc9119$',:password_confirmation=>'!UnCc9119$', :username => 'drittle',:email =>'drittle@uncc.edu'},

{:password=>'!UnCc3627$',:password_confirmation=>'!UnCc3627$', :username => 'crosenow',:email =>'crosenow@uncc.edu'},

{:password=>'!UnCc4713$',:password_confirmation=>'!UnCc4713$', :username => 'arufolo',:email =>'arufolo@uncc.edu'},

{:password=>'!UnCc5908$',:password_confirmation=>'!UnCc5908$', :username => 'nsaevang',:email =>'nsaevang@uncc.edu'},

{:password=>'!UnCc2577$',:password_confirmation=>'!UnCc2577$', :username => 'hschmid4',:email =>'hschmid4@uncc.edu'},

{:password=>'!UnCc6768$',:password_confirmation=>'!UnCc6768$', :username => 'jshah6',:email =>'jshah6@uncc.edu'},

{:password=>'!UnCc2909$',:password_confirmation=>'!UnCc2909$', :username => 'bsilva2',:email =>'bsilva2@uncc.edu'},

{:password=>'!UnCc9763$',:password_confirmation=>'!UnCc9763$', :username => 'dsubedi',:email =>'dsubedi@uncc.edu'},

{:password=>'!UnCc2626$',:password_confirmation=>'!UnCc2626$', :username => 'asubhan',:email =>'asubhan@uncc.edu'},

{:password=>'!UnCc0887$',:password_confirmation=>'!UnCc0887$', :username => 'btran7',:email =>'btran7@uncc.edu'},

{:password=>'!UnCc1004$',:password_confirmation=>'!UnCc1004$', :username => 'dpturnbu',:email =>'dpturnbu@uncc.edu'},

{:password=>'!UnCc4999$',:password_confirmation=>'!UnCc4999$', :username => 'dtyndal4',:email =>'dtyndal4@uncc.edu'},

{:password=>'!UnCc5720$',:password_confirmation=>'!UnCc5720$', :username => 'mwhitl18',:email =>'mwhitl18@uncc.edu'},

{:password=>'!UnCc8799$',:password_confirmation=>'!UnCc8799$', :username => 'mwong8',:email =>'mwong8@uncc.edu'},

{:password=>'!UnCc3484$',:password_confirmation=>'!UnCc3484$', :username => 'dxiong4',:email =>'dxiong4@uncc.edu'},

{:password=>'!UnCc3839$',:password_confirmation=>'!UnCc3839$', :username => 'ayang10',:email =>'ayang10@uncc.edu'},

{:password=>'!UnCc5749$',:password_confirmation=>'!UnCc5749$', :username => 'wyang10',:email =>'wyang10@uncc.edu'},

{:password=>'!UnCc0000$',:password_confirmation=>'!UnCc0000$', :username => 'dsequoia',:email =>'dsequoia@uncc.edu'},
{:password => '!UnCc0000$',:password_confirmation=>'!UnCc0000$', :username => 'scolli50', :email => 'scolli50@uncc.edu'},

{:password=>'!itcs!3155!', :first_name => 'Daniel', :last_name => 'Foglio',:password_confirmation=>'!itcs!3155!',:username=>'dfoglio', :email=>'dfoglio@uncc.edu'},
{:password=>'!itcs!3155!', :first_name => 'Dimitrios', :last_name => 'Arethas',:password_confirmation=>'!itcs!3155!',:username=>'darethas', :email=>'darethas@uncc.edu'},
{:password=>'!itcs!3155!', :first_name => 'Sean', :last_name => 'Gallagher',:password_confirmation=>'!itcs!3155!',:username=>'sgalla19', :email=>'sgalla19@uncc.edu'}
]

users.each do |u|
  User.create! do |entry|
    entry.username = u[:username]
    entry.email = u[:email]
    entry.password = u[:password]
    entry.password_confirmation = u[:password_confirmation] 
    if entry.password == '!itcs!3155!'
      entry.first_name = u[:first_name]
      entry.last_name = u[:last_name]
      entry.section = 0
      entry.student_id = 0
    end
  end
end

User.find_by_username('darethas').update_attribute :admin, true
User.find_by_username('sgalla19').update_attribute :admin, true
User.find_by_username('dfoglio').update_attribute :admin, true
User.create! :password=>'!itcs!3155!', :first_name => 'student', :last_name => 'student',:password_confirmation=>'!itcs!3155!',:username=>'student', :email=>'student@uncc.edu'


