server "cci-vm4-18.uncc.edu", :app, :web, :db, :primary => true
set :deploy_to, "/var/www/autograder"
set :branch, 'master'
set :keep_releases, 5