AutoGrader3155::Application.routes.draw do

  get "assignment_updates/index"

  root :to=> 'users#home', :via=>:get
  #devise_for :users, :skip => [:registrations] 
  devise_for :users, :skip => [:registrations] do
    get "/users/sign_out" =>"devise/sessions#destroy", :as => :destroy_user_session
  end
  #devise_scope :user do
   # get "/users/sign_out" =>"devise/sessions#destroy", :as => :destroy_user_session
  #end

  get 'users'=>'users#index'

  match 'user/:id'=>'users#show', :as=>'user', :via=>:get#, :path_names => {:show =>'Dan'}
  match 'user/:id/edit'=>'users#edit', :via=>:get, :as=>'user_edit'
  match 'user/:id/update'=>'users#update', :via=>:put, :as=>'user_update'

  match 'user/:user_id/problem/:id'=>'problems#index', :via=>:get, :as=>'submissions'
  match 'user/:user_id/problem/:id'=>'problems#submit', :via=>:post, :as=>'submit'

  match 'user/:user_id/submission/:id'=>'submissions#show', :via=>:get, :as=>'submission'
  match 'admin/user/:user_id/submission/:id'=>'submissions#download', :via=>:get, :as=>'dl'
  get "grades/summary"
  get "grades/full"

  match "announcements/:id/hide" => 'announcements#hide', as: 'hide_announcement'
  match "announcement/add" => 'assignment_updates#add', :via => :post, as: 'add_announcement'
  match "admin/assignments" => 'assignment_updates#index', :via => :get, :as => 'assignments'
  match "admin/assignments/:id/edit" => 'assignment_updates#edit', :via => :get, :as => 'edit_assignment'
  match "admin/assignments/:id" => 'assignment_updates#update', :via=>:put, as: 'update_assignment'
  match "admin/semester" => 'new_semesters#delete_db_data', via: :get, as: 'blow_up'
  match "admin/new/semester" => 'new_semesters#index', via: :get, as: 'semester'
  match "admin/new/roster"=> 'new_semesters#import_new_roster', via: :post, as: 'new_roster'
  match "admin/new/roster/example"=> 'new_semesters#csv_example', via: :get, as: 'csv_example'
  match "admin/new/reseed"=> 'new_semesters#reseed_db', via: :get, as: 'reseed'
  match "admin/assignments/hw0" => 'assignment_updates#hw0', :via=>:get, as: 'hw0'
  match "admin/assignments/hw1" => 'assignment_updates#hw1', :via=>:get, as: 'hw1'
  match "admin/assignments/hw2" => 'assignment_updates#hw2', :via=>:get, as: 'hw2'
  match "admin/assignments/hw3" => 'assignment_updates#hw3', :via=>:get, as: 'hw3'
  match "admin/assignments/hw4" => 'assignment_updates#hw4', :via=>:get, as: 'hw4'
  match 'hwsubmission/:user_id/hw2'=>'hw_submissions#hw2source', :via=>:post, :as=>'hw2source'
  match 'hwsubmission/dl/:id'=>'hw_submissions#ajax_file_dl', :via=>:post, :as=>'hw2dl'

 end
