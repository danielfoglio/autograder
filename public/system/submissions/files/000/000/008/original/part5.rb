#!/usr/bin/env irb

# USE CLASS EVAL INSTEAD OF OPENING CLASS AND ANDDING METHOD EX:
	# String.class_eval do
		#def palindrome?
			#self.gsub...
	#can be done with enumerable too
class Class
	def attr_accessor_with_history(attr_name)
		attr_name = attr_name.to_s
		attr_reader attr_name
		attr_reader attr_name+"_history"
		class_eval %Q{ def #{attr_name}=(args)
				  
				  @#{attr_name}= args
				  if @#{attr_name}_history.nil? then
				     @#{attr_name}_history = [nil]
			  		end
				     @#{attr_name}_history << args
			       end
			     }
	end
	
end

module Enumerable
	def palindrome?
		@rtr=self
		#@rtr = self.to_a unless self.is_a? String
		#@rtr = @rtr.join unless self.is_a? String
   		#@rtr = @rtr.gsub(/[\W]/,'').downcase
   		#@rtr == @rtr.reverse
		if @rtr.respond_to? :reverse then @rtr == @rtr.reverse 
		else  @rtr.to_a == @rtr.to_a.reverse end
	end
end

class Numeric
	@@currencies= {'in' => 1, 'dollar' => 1.0, 'yen' => 0.013, 'euro' => 1.292, 'rupee' => 0.019}
	def method_missing(method_id)
		#@hold = 0		
		singular_currency = method_id.to_s.gsub(/s$/, '')
		#args = args_ary[0].to_s.gsub(/s$/, '')
		if @@currencies.has_key?(singular_currency) then
			 self * @@currencies[singular_currency]
		
		else
			super
		end
		#hold *@@currencies[args] unless
	end

  def in(currency)
    self / 1.send(currency)
  end
end
	
end

class String
	
   	def palindrome?
		@rtr=self
		@rtr = self.to_a unless self.is_a? String
		@rtr = @rtr.join unless self.is_a? String
   		@rtr = @rtr.gsub(/[\W]/,'').downcase
   		@rtr == @rtr.reverse
	end

end

