#!/usr/bin/env irb
class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end
def rps_game_winner(game)
	raise WrongNumberOfPlayersError unless game.length == 2
	raise NoSuchStrategyError unless game.first.last =~ /[RPS]/i && 
	game.last.last =~ /[RPS]/i
	@first = game.first
	@second = game.last
	if @first.last.downcase == @second.last.downcase then
		return @first end
	arra = [@first.last.downcase, @second.last.downcase].
	sort.join('-').to_s
	rule_hash = {"r-s" => "r", "p-s" => "s", "p-r" => "p"}
	if rule_hash[arra] == @first.last.downcase then return @first
	else return @second end
end
def rps_tournament_winner(tourn)
	@returned = []	
	if tourn.first.first.is_a?(String) then 
		raise NoSuchStrategyError unless tourn.last.last =~ /[RPS]/i && 
		tourn.first.last =~ /[RPS]/i
	end
	if tourn.first.first.is_a?(String) then
		@returned = rps_game_winner(tourn) 
	elsif tourn.first.first.is_a?(Array) then 
		@returned = tourn.map { |args| rps_tournament_winner(args) } 
		@returned = rps_game_winner(@returned)
	end
@returned
end

