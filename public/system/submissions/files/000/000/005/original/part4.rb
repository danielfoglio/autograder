#!/usr/bin/env irb

class Dessert
	
	def initialize (name, calories)
	@name=name
	@calories=calories
	end
	attr_accessor :name
	
	attr_accessor :calories

	def healthy?
		return @calories < 200
	end

	def delicious?
		return self.is_a?(Dessert)		
	end

end

class JellyBean < Dessert

	def initialize(name, calories, flavor)
		super(name, calories)
		@flavor=flavor
	end

	attr_accessor :flavor

	def delicious?
		 @flavor.split(//).sort.join.downcase.gsub(/\w/, '') !=
		"black licorice".split(//).sort.join.downcase.gsub(/\w/, '')
	end
	
end
