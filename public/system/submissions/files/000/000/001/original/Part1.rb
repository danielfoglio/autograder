#!/usr/bin/env ruby

# Part 1 (a)
def palindrome?(str)
   @rtr=str.gsub(/[\W]/,"").downcase
   return @rtr == @rtr.reverse
end





# Part 1 (b)
def count_words(str)
   @haash=Hash.new(0)
   str.downcase.scan(/\w+/) { |key| @haash[key]+=1}
   return @haash
end  
   
