#!/usr/bin/env ruby
uri = "# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 0) do

  create_table "buyer", :primary_key => "buyer_id", :force => true do |t|
    t.string  "fname",           :limit => 15,                                :null => false
    t.string  "minit",           :limit => 1
    t.string  "lname",           :limit => 15,                                :null => false
    t.string  "phone",           :limit => 10,                                :null => false
    t.string  "email"
    t.decimal "max_price_range",               :precision => 12, :scale => 2
  end

  create_table "employee", :primary_key => "employee_id", :force => true do |t|
    t.string  "fname",      :limit => 15,                          :null => false
    t.string  "minit",      :limit => 1
    t.string  "lname",      :limit => 15,                          :null => false
    t.string  "ssn",        :limit => 9
    t.date    "bdate"
    t.string  "address"
    t.string  "phone",      :limit => 10,                          :null => false
    t.string  "superssn",   :limit => 9,  :default => "000000000", :null => false
    t.integer "emp_office"
  end

  add_index "employee", ["emp_office"], :name => "emp_office"

  create_table "office", :primary_key => "office_no", :force => true do |t|
    t.string "state",   :limit => 2,  :null => false
    t.string "phone",   :limit => 10, :null => false
    t.string "address"
  end

  create_table "owners", :primary_key => "owner_id", :force => true do |t|
    t.string "fname",   :limit => 15, :null => false
    t.string "minit",   :limit => 1
    t.string "lname",   :limit => 15, :null => false
    t.string "phone",   :limit => 10, :null => false
    t.string "address"
    t.string "email"
  end

  create_table "property", :primary_key => "property_id", :force => true do |t|
    t.string  "state",         :limit => 2
    t.decimal "asking_p",                    :precision => 12, :scale => 2, :null => false
    t.string  "address"
    t.string  "owned_by",      :limit => 10
    t.string  "supervised_by", :limit => 10
    t.integer "bedroom"
    t.decimal "bathroom",                    :precision => 2,  :scale => 2
    t.integer "sq_foot"
    t.string  "on_market",     :limit => 1
  end

  add_index "property", ["owned_by"], :name => "owned_by"
  add_index "property", ["supervised_by"], :name => "supervised_by"

  create_table "sold_to", :id => false, :force => true do |t|
    t.integer "prop",                                                        :default => 0,  :null => false
    t.string  "buyers",         :limit => 10,                                :default => "", :null => false
    t.date    "date_purchased",                                                              :null => false
    t.decimal "selling_p",                    :precision => 12, :scale => 2
    t.decimal "asking_p",                     :precision => 12, :scale => 2
  end

  add_index "sold_to", ["prop"], :name => "prop"

end
"
require "rspec"
require 'pathname'
require "nokogiri"
require 'rubygems'
require 'mechanize'
require 'uri'
                                                   

                    
#uri = "http://growing-moon-5313.herokuapp.com"
uri = URI.parse(uri) if uri
host = URI::HTTP.build(:host => uri.host, :port => uri.port).to_s if uri
$url = URI.join(host, 'movies').to_s

# Logic to parse column HEADER from a html TABLE.
# from http://stackoverflow.com/questions/8749101/parse-html-table-using-nokogiri-and-mechanize
class TableExtractor  
  def self.extract_column table, header
    table.search('tbody/tr').collect do |row|
      case header
      when :title
        row.at("td[1]").text.strip
      when :rating
        row.at("td[2]").text.strip
      when :release_date
        row.at("td[3]").text.strip
      when :more_info
        row.at("td[4]").text.strip
      end
    end
  end
end

describe "App" do
  it "should respond to simple request [0 points]" do
    agent = Mechanize.new
    page = agent.get($url)
  end
end

def select_all_ratings_and_submit(page)
  # This block of code is to account for students who left the ratings unchecked by default, and therefore not displaying anything
  page.forms.each do |form|
    form.checkboxes.each do |cb|
      cb.check
    end
    submit = form.button_with(:id => 'ratings_submit')
    if submit
      page = form.submit(submit)
    else
      page = form.submit
    end
  end
  page
end

def get_sort_link(page, field)
  id = "#{field}_header"
  link = page.link_with(:id => id)
  return link if link

  if not page.search("##{id}").empty?
    href = page.search("##{id}")[0].search('a')[0]['href']
    link = page.link_with(:href => href)
    return link if link
  end

  raise "Cannot find sort link"
end

def sorted?(column)
  column = column.select{|x| x != ""}.# gets rid of that Amelie blank release date field
    map{|x| x.downcase.gsub(/[^a-z0-9]/, ' ').strip}
  column.should_not be_empty
  column.should == column.sort
end


describe "Table header" do
  before(:each) do
    @startpage = Mechanize.new.get($url)
    @startpage = select_all_ratings_and_submit(@startpage)
  end
  it "should have link to sort by title [10 points]" do
    page = @startpage.link_with(:id => 'title_header')
    page.should_not be_nil
  end
  it "should have link to sort by release date [10 points]" do
    page = @startpage.link_with(:id => 'release_date_header')
    page.should_not be_nil
  end
end

describe "Table" do
  before(:each) do
    @startpage = Mechanize.new.get($url)
    @startpage = select_all_ratings_and_submit(@startpage)
  end
  it "should be sortable by title [20 points]" do
    sorted_page = get_sort_link(@startpage, 'title').click
    table = sorted_page.parser.css("#movies").first
    column = TableExtractor.extract_column table, :title
    sorted?(column).should be_true

  end
  it "should be sortable by release date [20 points]" do
    sorted_page = get_sort_link(@startpage, 'release_date').click
    table = sorted_page.parser.css("#movies").first
    column = TableExtractor.extract_column table, :release_date
    sorted?(column).should be_true
  end
  # This is not actually part of the spec
  #it "should highlight neither header by default" do
  #  title = @startpage.parser.css('#title_header')
  #  release_date = @startpage.parser.css('#release_date_header')
  #  # TODO check css styling
  #end
  it "should highlight title header when sorted [20 points]" do
    sorted_page = get_sort_link(@startpage, 'title').click
    title = sorted_page.parser.css('#title_header')
    release_date = sorted_page.parser.css('#release_date_header')
    # TODO check css styling
    sorted_page.search('table[@id=movies]/thead/tr/th[1]')[0].attributes['class'].value.should =~ /\bhilite\b/
  end
  it "should highlight release date header when sorted [20 points]" do
    sorted_page = get_sort_link(@startpage, 'release_date').click
    title = sorted_page.parser.css('#title_header')
    release_date = sorted_page.parser.css('#release_date_header')
    # TODO check css styling
    sorted_page.search('table[@id=movies]/thead/tr/th[3]')[0].attributes['class'].value.should =~ /\bhilite\b/
  end
end
