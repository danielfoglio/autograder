describe "BookInStock" do
  it "should be defined" do
    lambda { BookInStock.new }.should_not raise_error(::NameError)
  end

  it "should be able to set and get a book's isbn and price [50 points]" do
    book = BookInStock.new('isbn1', 33.8)
    book.isbn.should eq( 'isbn1')
    book.price.should eq (33.8)
    book.isbn = 'isbn2'
    book.isbn.should eq( "isbn2")
    book.price = 300.00
    book.price.should eq(300.0)
  end
end
describe "#price_in_cents" do
  it "should be defined" do
    lambda { price_in_cents }.should_not raise_error(::NoMethodError)
  end

  it "should correctly display a books price in cents(if your number has several 0's and a 1 after try the round method! Thanks James) [50 points]" do
    b = BookInStock.new('isbn11', 10.5)
    b.price_in_cents.should eq(1050.0)
    b = BookInStock.new('isbn22', 1.1)
    b.price_in_cents.round.should eq(110)
    b = BookInStock.new('isbn22', 1.12)
    b.price_in_cents.round.should eq(112)
  end
end

