require 'rspec'
require 'rspec/mocks'

def out_it(&block)
  original_stdout = $stdout
  $stdout = fake = StringIO.new
  begin
    yield
  ensure
    $stdout = original_stdout
  end
    fake.string
end

describe "#hello" do
  it "should be defined" do
    lambda { hello("Testing") }.should_not raise_error(::NoMethodError)
  end

  it "The hello method prints the correct string [30 points]" do
    o = out_it {hello('Dan')}
    o.should == "Hello, Dan\n"
    
    d = out_it {hello('Mr. Ilson')}
    d.should == "Hello, Mr. Ilson\n"

    e = out_it {hello('YALLLL')}
    e.should == "Hello, YALLLL\n"

  end
end
describe "#starts_with_consonant?" do
  it "should be defined" do
    lambda { starts_with_consonant?("d") }.should_not raise_error(::NoMethodError)
  end
  it "The starts_with_consonant? method returns the correct boolean [30 points]" do
    starts_with_consonant?("asdfgh").should_not be_true, "Incorrect results for input: \"asdfgh\""
    starts_with_consonant?("Veeeeeeee").should be_true, "Incorrect results for input: \"Veeeeeeee\""
    starts_with_consonant?("vest").should be_true, "Incorrect results for input: \"vest\""
    starts_with_consonant?("acrypt").should_not be_true, "Incorrect results for input: \"acrypt\""
  end
end
describe "#binary_multiple_of_4?" do
  it "should be defined" do
    lambda { binary_multiple_of_4?("yes") }.should_not raise_error(::NoMethodError)
  end
  it "The binary_multiple_of_4? method returns the correct boolean [40 points]" do
    binary_multiple_of_4?("111111101").should_not be_true, "Incorrect results for input: \"111111101\""

    binary_multiple_of_4?("1010101010100").should be_true, "Incorrect results for input: \"1010101010100\""
    binary_multiple_of_4?("0101010101010100").should be_true, "Incorrect results for input: \"0101010101010100\""

    binary_multiple_of_4?("1000000000001").should_not be_true, "Incorrect results for input: \"1000000000001\""
    binary_multiple_of_4?("a000000000001").should_not be_true, "Incorrect results for input: \"a000000000001\""

  end
end
