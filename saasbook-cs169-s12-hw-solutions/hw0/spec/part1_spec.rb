3

describe "#sum" do
  it "should be defined" do
    lambda { sum([1,3,4]) }.should_not raise_error(::NoMethodError)
  end

  it "The sum method returns the correct sum [30 points]" do
    sum([1,2,3,4,5]).class.should == Fixnum
    sum([1,2,3,4,5]).should eq(15), "Incorrect results for input: [1,2,3,4,5]"
    sum([1,2,3,4,-5]).should eq(5), "Incorrect results for input: [1,2,3,4,-5]"
    sum([1,2,3,4,-5,5,-100]).should eq(-90), "Incorrect results for input: [1,2,3,4,-5,5,-100]"
  end
end
describe "#max_2_sum" do
  it "should be defined" do
    lambda { max_2_sum([1,2,3]) }.should_not raise_error(::NoMethodError)
  end
  it "The max_2_sum method returns the correct sum [30 points]" do
    max_2_sum([1,2,3,4,5]).class.should == Fixnum
    max_2_sum([1,2,3,4,100]).should eq(104), "Incorrect results for input: [1,2,3,4,100]"
    max_2_sum([1,-2,-3,-4,-5]).should eq(-1), "Incorrect results for input: [1,-2,-3,-4,-5]"
    max_2_sum([1,2,3,4,-5,5,-100,100]).should eq(105), "Incorrect results for input: [1,2,3,4,-5,5,-100,100]"
  end
end
describe "#sum_to_n?" do
  it "should be defined" do
    lambda { sum_to_n?([1,2,3]) }.should_not raise_error(::NoMethodError)
  end
  it "The sum_to_n? method returns the correct value [40 points]" do
    sum_to_n?([1,2,3,4,5], 5).should eq(true), "Incorrect results for input: [1,2,3,4,5], 5"
    sum_to_n?([1,2,5,6,7,8], 3).should eq(true), "Incorrect results for input: [1,2,5,6,7,8], 3"
    sum_to_n?([100,50,50,2,100,4,5], 100).should eq(true), "Incorrect results for input: [100,50,50,2,100,4,5], 100"
    sum_to_n?([-1,-2,3,4,5,-8], -3).should eq(true), "Incorrect results for input: [-1,-2,3,4,5,-8], -3"
    sum_to_n?([1,2,3,4,5], -3).should eq(false), "Incorrect results for input: [1,2,3,4,5], -3"
    sum_to_n?([], -3).should eq(false), "Incorrect results for input: [], -3"
    sum_to_n?([-3], -3).should eq(false), "Incorrect results for input: [], -3"

  end
  it "The sum_to_n? method returns the correct value for difficult cases - no points deducted for failure" do
    sum_to_n?([], 5).should eq(false), "Incorrect results for input: [], 5"
    sum_to_n?([4], 8).should eq(false), "Incorrect results for input: [4], 8"
    sum_to_n?([4,4], 8).should eq(true), "Incorrect results for input: [4,4], 8"


  end
end
