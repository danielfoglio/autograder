# 30% of total
describe "single attr_accessor_with_history" do
  before(:each) do
    class Course
      attr_accessor_with_history :instructor
    end
  end
  it "should return nil as the first element [6 points]" do
    cs169 = Course.new
    cs169.instructor = "Armando Fox"

    cs169.instructor_history[0].should be_nil
  end
  it "should record all past values of the variable, and be able to record for multiple instances [9 points]" do
    cs169 = Course.new
    cs188 = Course.new

    cs169.instructor = "Armando Fox"
    cs188.instructor = "Dan Klein"
    cs188.instructor = "Pieter Abbeel"

    cs169.instructor_history[0].should be_nil
    cs188.instructor_history[0].should be_nil
    cs188.instructor_history[1].should eq("Dan Klein")
  end
  it "should record all past values of the variable, and be able to record for multiple variables [15 points]" do
    cs169 = Course.new
    cs188 = Course.new

    cs169.instructor = "Armando Fox"
    cs169.instructor = "Fox Armando"
    cs188.instructor = "Dan Klein"
    cs188.instructor = "Pieter Abbeel"

    cs169.instructor_history[0].should be_nil
    cs169.instructor_history[1].should eq("Armando Fox"), "Incorrect instructor"
    cs188.instructor_history[0].should be_nil
    cs188.instructor_history[1].should eq("Dan Klein"),  "Incorrect instructor"
  end
end

describe "multiple attr_accessor_with_history" do
  before(:each) do
    class Course
      attr_accessor_with_history :instructor
      attr_accessor_with_history :year
    end
  end
  it "should return nil as the first element [14 points]" do
    cs169 = Course.new
    cs169.instructor = "Armando Fox"

    cs169.instructor_history[0].should be_nil
  end
  it "should record all past values of the variable, and be able to record for multiple instances [21 points]" do
    cs169 = Course.new
    cs188 = Course.new

    cs169.instructor = "Armando Fox"
    cs188.instructor = "Dan Klein"
    cs188.instructor = "Pieter Abbeel"

    cs169.instructor_history[0].should be_nil
    cs188.instructor_history[0].should be_nil
    cs188.instructor_history[1].should eq("Dan Klein")
  end
  it "should record all past values of the variable, and be able to record for multiple variables [35 points]" do
    cs169 = Course.new
    cs188 = Course.new
    cs169.instructor = "Armando Fox"
    cs169.instructor = "Fox Armando"
    cs169.year = 2011
    cs169.year = 2012
    cs188.instructor = "Dan Klein"
    cs188.instructor = "Pieter Abbeel"
    cs188.year = 2009
    cs188.year = 2010
    cs188.year = 2011
    cs188.year = 2012

    cs169.instructor_history[0].should be_nil
    cs169.instructor_history[1].should eq("Armando Fox")
    cs188.instructor_history[0].should be_nil
    cs188.instructor_history[1].should eq("Dan Klein")
    cs169.year_history[0].should be_nil
    cs169.year_history[1].should eq(2011)
    cs188.year_history[0].should be_nil
    cs188.year_history[1].should eq(2009)
    cs188.year_history[2].should eq(2010)
    cs188.year_history[3].should eq(2011)
  end
end

class Numeric
  def between?(low, high)
    return self >= low && self <= high
  end
end

# 30/100 pts for part 1
describe "Currency conversion" do

  # 2 points for each of the singular forms
  it "correctly converts currency from rupees to dollars (singular) [2 points]" do
    2.rupee.in(:dollar).should be_close(0.038,0.001)
  end
  it "correctly converts currency from yen to dollars (singular) [2 points]" do
    3.yen.in(:dollar).should be_close(0.039,0.001)
  end
  it "correctly converts currency from euro to dollars (singular) [2 points]" do
    6.euro.in(:dollar).should be_close(7.755,0.005)
  end

  # 5 pts for each of the plural forms
  it "correctly converts currency from rupees to dollars (plural) [3 points]" do
    2.rupees.in(:dollars).should be_close(0.038,0.001)
  end
  it "correctly converts currency from yen to dollars (plural) [3 points]" do
    3.yen.in(:dollars).should be_close(0.039,0.001)
  end
  it "correctly converts currency from euro to dollars (plural) [3 points]" do
    6.euros.in(:dollars).should be_close(7.755,0.005)
  end

  # 19 points for integration testing
  it "correctly converts currency from rupees to yen, euros to rupees, yen to euros [15 points]" do
    5.rupees.in(:yen).should be_close(7.3,0.1)
    0.39.euros.in(:rupees).should be_close(26.55,0.05)
    1146.yen.in(:dollars).should be_close(14.9,0.05)
  end
end

# 30/100 pts for part 2
describe "adapted palindrome?" do
  it "correctly identifies positive and negative palindromes [30 points]" do
    "a man a plan a canal panama".palindrome?.should be_true
    "a man a plan a canal panama".should be_a_palindrome
    "Rac~~~Ec:A!r".should be_a_palindrome
    "hello World!".should_not be_a_palindrome
    "Computer Science 169 961 ecneics Retupmoc".should be_a_palindrome
    "computer science !169!".should_not be_a_palindrome
  end
end

class ValidPalindromeTest
  include Enumerable
  def initialize (a, b)
    @a = a
    @b = b
  end

  def each(&block)
    if block
      (@a..@b).each{ |m| block.yield m }
      ((@b*-1)..(@a*-1)).each{ |m| block.yield -1 * m }
    else
      enum_for(:each)
    end
  end
end

# 40/100 pts for part 3
describe "enumerable palindrome?" do
  
  it "correctly identifies simple array positive palindromes [6 points]" do
    ["a", "b", "c", "b", "a"].should be_a_palindrome
    [1,2,3,4,3,2,1].should be_a_palindrome
  end
  it "correctly identifies simple array non-palindromes [6 points]" do
    [1,2,3,4,3,2].should_not be_a_palindrome
    ["a","b"].should_not be_a_palindrome
  end
  it "should not error on non-sensical cases of enumerables, like hashes [5 points]" do
    pvalue = {"hello"=> "world"}.palindrome?
    (pvalue.is_a?(TrueClass) || pvalue.is_a?(FalseClass)).should be_true
    
    pvalue2 = {1=> 2, 3=> 4, 5=> 4, 4=> 10}.palindrome?
    (pvalue2.is_a?(TrueClass) || pvalue2.is_a?(FalseClass)).should be_true
  end
  it "should still work for the case of non-array enumerables that do make sense, like iterators (valid palindrome) [15 points]" do
    ValidPalindromeTest.new(1,9).should be_a_palindrome
    ValidPalindromeTest.new(81,169).should be_a_palindrome
  end

  it "should still work for non-array enumerables that do make sense (invalid palindromes) [8 points]" do
    (1..2).should_not be_a_palindrome
    (1..8).should_not be_a_palindrome
  end
end

