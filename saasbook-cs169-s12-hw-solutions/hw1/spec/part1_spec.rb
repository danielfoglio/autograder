describe "#palindrome?" do
  it "should be defined" do
    lambda { palindrome?("Testing") }.should_not raise_error(::NoMethodError)
  end

  it "recognizes palindromes correctly [25 points]" do
    palindrome?("A man, a plan, a canal -- Panama").should be_true, "Incorrect results for input: \"A man, a plan, a canal -- Panama\""
    palindrome?("Madam, I'm Adam!").should be_true, "Incorrect results for input: \"Madam, I'm Adam!\""
    palindrome?("Do geese see God?").should be_true, "Incorrect results for input: \"Do geese see God?\""
    palindrome?("Murder for a jar of red rum.").should be_true, "Incorrect results for input: \"Murder for a jar of red rum.\""
    palindrome?("Never odd or even.").should be_true , "Incorrect results for input: \"Never odd or even.\""
    palindrome?("Racecar").should be_true, "Incorrect results for input: \"Racecar\""
    palindrome?("Was it a car or a cat i saw").should be_true, "Incorrect results for input: \"Was it a car or a cat i saw\""
  end

  it "recognizes non palindromes correctly [25 points]" do
    palindrome?("This is not a palindrome").should be_false, "Incorrect results for input: \"This is not a palindrome\""
    palindrome?("Trolls, ogres, orcs").should be_false, "Incorrect results for input: \"Trolls, ogres, orcs\""
    palindrome?("Lorem ipsum dolor sit amet.").should be_false, "Incorrect results for input: \"Lorem ipsum dolor sit amet.\""
    palindrome?("There's some punctuation up in here?!").should be_false, "Incorrect results for input: \"There's some punctuation up in here?!\""
end

end

describe "#count_words" do
  it "should be defined" do
    lambda { count_words("Testing") }.should_not raise_error(::NoMethodError)
  end

  it "should return a Hash" do
    count_words("Testing").class.should == Hash
  end

  it "counts the words properly [40 points]" do
    sample1 = count_words "what the what the what"
    sample1["what"].should eq(3), "Incorrect count for word: what"
    sample1["the"].should eq(2), "Incorrect count for word: the"

    sample2 = count_words "blah blah blah blah blah" 
    sample2.should eq({"blah" => 5}) , "Incorrect results for input \"blah blah blah blah blah\"" 

    sample3 = count_words "thats this and thats that"
    sample3.should eq({"thats" => 2, "this" => 1, "and" => 1, "that" => 1}),  "Incorrect results for input: \"thats this and thats that\""
  end

  it "is not sensitive to case [10 points]" do
    sample1 = count_words "there is UPPER upper case"
    sample1["upper"].should eq(2) , "Case should not matter for counting words"
  end
end
