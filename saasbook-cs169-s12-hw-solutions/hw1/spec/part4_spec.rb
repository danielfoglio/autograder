describe "dessert" do
  it "should be able to set and get a dessert's name and calories [20 points]" do
    dessert = Dessert.new('cake', 500)
    dessert.name.should eq( 'cake')
    dessert.calories.should eq (500)
    dessert.name = 'cupcake'
    dessert.calories = 300
    dessert.name.should eq('cupcake')
    dessert.calories.should eq(300)
  end

  it "should return true if a dessert has fewer than 200 calories [10 points]" do
    dessert1 = Dessert.new('apple', 100).should be_healthy, "Dessert with fewer than 200 calories returned incorrect healthy value"
    dessert2 = Dessert.new('banana', 115).should be_healthy, "Dessert with fewer than 200 calories returned incorrect healthy value"
    dessert3 = Dessert.new('milkshake', 1000).should_not be_healthy, "Dessert with more than 200 calories returned incorrect healthy value"
  end

  it "should return true for any dessert [20 points]" do
    dessert1 = Dessert.new('apple', 100).should be_delicious
    dessert2 = Dessert.new('banana', 115).should be_delicious
    dessert3 = Dessert.new('milkshake', 1000).should be_delicious
  end
end

describe "jellybean" do
  it "should be a subclass of Dessert [10 points]" do
    JellyBean.superclass.should eq(Dessert), "Incorrect superclass"
  end

  it "should be able to set and get a jellybean's flavor [20 points]" do
    jellybean = JellyBean.new('jellybean', 5, 'strawberry')
    jellybean.flavor.should eq('strawberry')
    jellybean.flavor = 'lemon'
    jellybean.flavor.should eq('lemon')
  end

  it "should return true unless the flavor is black licorice [20 points]" do
    jellybean1 = JellyBean.new('jellybean', 5, 'strawberry').should be_delicious
    jellybean2 = JellyBean.new('jellybean', 5, 'lemon').should be_delicious
    jellybean3 = JellyBean.new('jellybean', 5, 'black licorice').should_not be_delicious
  end
end
