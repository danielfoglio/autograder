describe "anagrams" do
  it "should handle an empty array [2 points]" do
    anags = []
    result = combine_anagrams anags
    result.deepsort.should == []
  end

  it "should handle the most simple, single-character case with no repeats [2 points]" do
    anags = ['a', 'b', 'c']
    result = combine_anagrams anags
    result.should have(3).items
    result.deepsort.should include ['a'], ['b'], ['c']
  end

  it "should handle the most simple, single-character case with capital letters but no repeats [2 points]" do
    anags = ['a', 'B', 'c']
    result = combine_anagrams anags
    result.should have(3).items
    result.deepsort.should include ['a'], ['B'], ['c']
  end

  it "should handle a set with no repeated anagrams and words with distinct letters [10 points]" do
    anags = ['this', 'is', 'a', 'test']
    result = combine_anagrams anags
    result.should have(4).items
    result.deepsort.should include ['this'], ['is'], ['a'], ['test']
  end

  it "should handle a set with no repeated anagrams, but words with similar letters [2 points]" do
    anags = ['aabbcc', 'abc']
    result = combine_anagrams anags
    result.should have(2).items
    result.deepsort.should include ['aabbcc'], ['abc']

    anags = ['aabbcc', 'aabbc']
    result = combine_anagrams anags
    result.should have(2).items
    result.deepsort.should include ['aabbcc'], ['aabbc']
  end

  it "should handle non-anagrams that have all but one letters the same [2 points]" do
    anags = ['reads', 'dear']
    result = combine_anagrams anags
    result.should have(2).items
    result.deepsort.should include ['reads'], ['dear']
  end

  it "should handle a complicated case of some repeated anagrams, but no duplicates [25 points]" do
    anags = ['cars', 'racs', 'four', 'for', 'scar', 'potatoes', 'creams', 'scream']
    result = combine_anagrams anags
    result.should have(5).items
    result.deepsort.should include ['cars', 'racs', 'scar'].sort, ['four'], ['for'], ['potatoes'], ['creams', 'scream'].sort
  end

  it "should handle a complicated case of some repeated anagrams, but no duplicates [25 points]" do
    anags = ['rat', 'panes', 'napes', 'dart', 'tard', 'tar', 'scar', 'snape']
    result = combine_anagrams anags
    result.should have(4).items
    result.deepsort.should include ['rat', 'tar'].sort, ['panes', 'napes', 'snape'].sort, ['dart', 'tard'].sort, ['scar']
  end

  it "should handle a simple case of some repeated single-character inputs [2 points]" do
    anags = ['a', 'a', 'b', 'b', 'c', 'd']
    result = combine_anagrams anags
    result.should have(4).items
    result.deepsort.should include ['a', 'a'], ['b', 'b'], ['c'], ['d']
  end

  it "should handle a complicated case of some repeated anagrams, with duplicate inputs [7 points]" do
    anags = ['cars', 'racs', 'four', 'for', 'scar', 'potatoes', 'creams', 'scream', 'scream', 'cars']
    result = combine_anagrams anags
    result.should have(5).items
    result.deepsort.should include ['cars', 'cars', 'racs', 'scar'].sort, ['four'], ['for'], ['potatoes'], ['creams', 'scream', 'scream'].sort
  end

  it "should handle a complicated case of some repeated anagrams, with duplicate inputs [7 points]" do
    anags = ['tops', 'pots', 'spot', 'spots', 'stop', 'tops', 'sausage', 'stops']
    result = combine_anagrams anags
    result.should have(3).items
    result.deepsort.should include ['pots', 'spot', 'stop', 'tops', 'tops'].sort, ['spots', 'stops'].sort, ['sausage']
  end

  it "should treat single-character capital letters as equal [2 points]" do
    anags = ['a', 'A']
    result = combine_anagrams anags
    result.deepsort.should == [['A', 'a'].sort]
  end

  it "should handle repeated single-character inputs, and treat captial letters as anagrams, while preserving case in the output [2 points]" do
    anags = ['a', 'a', 'A', 'a', 'D', 'b', 'b', 'c', 'd']
    result = combine_anagrams anags
    result.should have(4).items
    result.deepsort.should include ['a', 'a', 'a', 'A'].sort, ['b', 'b'], ['c'], ['D', 'd'].sort
  end

  it "should treat two identical words with different cases as the same, and preserve case in output [2 points]" do
    anags = ['hello', 'HeLLo']
    result = combine_anagrams anags
    result.deepsort.should == [['HeLLo', 'hello'].sort]
  end

  it "should treat many identical words with different cases as the same, and preserve case in output [8 points]" do
    anags = ['tOps', 'potS', 'spot', 'spots', 'stop', 'tops', 'sAUsage', 'STOPS']
    result = combine_anagrams anags
    result.should have(3).items
    result.deepsort.should include ['potS', 'spot', 'stop', 'tOps', 'tops'].sort, ['STOPS', 'spots'].sort, ['sAUsage']
  end
end

module Enumerable
  def deepsort
    self.map(&:sort)
  end
end

