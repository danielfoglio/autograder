describe "CartesianProduct" do
  it "should work for the first example given in the homework [15 points]" do
    c = CartesianProduct.new([:a,:b], [4,5])
    c.to_a.should include([:a, 4],[:a,5],[:b,5],[:b,4])
  end

  it "should work for the second example given in the homework [10 points]" do
    c = CartesianProduct.new([:a,:b], [])
    c.to_a.should have(0).items
  end

  it "should return nothing for the case where both of them are empty [15 points]" do
    c = CartesianProduct.new([], [])
    c.to_a.should have(0).items    
  end

  it "should work for other examples for 2x2 [20 points]" do
    c = CartesianProduct.new([11, 12], [13, 14])
    c.to_a.should include([11, 13],[11, 14],[12, 13],[12, 14])
   
    c2 = CartesianProduct.new([:x, :y], [:z, :w])
    c2.to_a.should include([:x, :z],[:x, :w],[:y, :z],[:y, :w])
  end

  it "should work for 3x3 and 4x4 [40 points]" do
    c = CartesianProduct.new([1,2,3], [4,5,6])
    c.to_a.should include([1, 4],[1, 5],[1, 6],[2, 4],[2, 5],[2, 6],[3, 4],[3, 5],[3, 6])

    c2 = CartesianProduct.new([1,2,3,4], [5,6,7,8])
    c2.to_a.should include([1, 5],[1, 6],[1, 7],[1, 8],[2, 5],[2, 6],[2, 7],[2, 8],[3, 5],[3, 6],[3, 7],[3, 8],[4, 5],[4, 6],[4, 7],[4, 8])
  end
end
