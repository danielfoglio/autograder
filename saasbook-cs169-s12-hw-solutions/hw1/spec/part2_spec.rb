# 30 pts for part A
describe "#rps_game_winner" do
  it "should be defined" do
    lambda { rps_game_winner() }.should_not raise_error(::NoMethodError)
  end

  # this is given for free in the outline
  it "should raise WrongNumberOfPlayersError if there are not exactly two players [1 point]" do
    lambda { rps_game_winner([ ["Allen", "S"] ]) }.should raise_error(WrongNumberOfPlayersError), "No error raised for incorrect number of players"
  end

  it "should raise NoSuchStrategyError if there is some strategy that is not R, P, or S [4 points]" do
    lambda { rps_game_winner([ ["Allen", "A"], ["Richard", "P"] ]) }.should raise_error(NoSuchStrategyError), "No error raised for invalid strategy"
  end

  it "should return the correct winner in a simple RPS game with a clear winner [15 points]" do
    rps_game_winner([ ["Dave", "P"], ["Armando", "S"] ])[0].should eq("Armando"), "Incorrect Winner returned" 
    rps_game_winner([ ["Richard", "R"], ["Michael", "P"] ])[0].should eq("Michael"), "Incorrect Winner returned" 
    rps_game_winner([ ["Richard", "S"], ["Allen", "R"] ])[0].should eq("Allen"), "Incorrect Winner returned" 
  end

  it "should return the first player in the case of a tie [10 points]" do
    rps_game_winner([ ["Allen", "P"], ["Richard", "P"] ])[0].should eq("Allen"), "Incorrect Winner returned"
    rps_game_winner([ ["David", "S"], ["Omer", "S"] ])[0].should eq("David"), "Incorrect Winner returned" 
    rps_game_winner([ ["Dave", "R"], ["Armando", "R"] ])[0].should eq("Dave"), "Incorrect Winner returned" 
  end
end

# 70 pts for part B
describe "#rps_tournament_winner" do
  it "should be defined" do
    lambda { rps_tournament_winner() }.should_not raise_error(::NoMethodError)
  end

  it "should still be able to handle the case where a tournament is just one game [10 points]" do
    tournament2 = [ [ "Armando", "P" ], [ "Dave", "S" ] ]
    rps_tournament_winner(tournament2)[0].should eq( "Dave")
  end

  it "should pass the example given in the homework of an 8-player tournament [5 points]" do
    tournament_ex = [
                     [
                      [ ["Armando", "P"], ["Dave", "S"] ],
                      [ ["Richard", "R"], ["Michael", "S"] ],
                     ],
                     [
                      [ ["Allen", "S"], ["Omer", "P"] ],
                      [ ["David E.", "R"], ["Richard X.", "P"] ]
                     ]
                    ]
    rps_tournament_winner(tournament_ex)[0].should == "Richard"
  end

  it "should pass a basic test case of 8 players [15 points]" do
    tournament8 = [
                   [
                    [
                     [ "Player 1", "R" ], [ "Player 2", "S" ]
                    ],
                    [
                     [ "Player 3", "P" ], [ "Player 4", "S" ]
                    ]
                   ],
                   [
                    [
                     [ "Player 5", "P" ], [ "Player 6", "R" ]
                    ],
                    [
                     [ "Player 7", "S" ], [ "Player 8", "R" ]
                    ]
                   ]
                  ]
    rps_tournament_winner(tournament8)[0].should == "Player 5"
  end

  it "should return the correct winner in the cases of 16 and 32-man tournaments [40 points]" do
    tournament32 = [
                    [
                     [
                      [
                       [
                        [ "Player 1", "R" ], [ "Player 2", "S" ]
                       ],
                       [
                        [ "Player 3", "P" ], [ "Player 4", "S" ]
                       ]
                      ],
                      [
                       [
                        [ "Player 5", "P" ], [ "Player 6", "R" ]
                       ],
                       [
                        [ "Player 7", "S" ], [ "Player 8", "R" ]
                       ]
                      ]
                     ],

                     [
                      [
                       [
                        [ "Player 9", "P" ], [ "Player 10", "R" ]
                       ],
                       [
                        [ "Player 11", "S" ], [ "Player 12", "S" ]
                       ]
                      ],
                      [
                       [
                        [ "Player 13", "P" ], [ "Player 14", "S" ]
                       ],
                       [
                        [ "Player 15", "R" ], [ "Player 16", "P" ]
                       ]
                      ]
                     ]
                    ],
                    [
                     [
                      [
                       [
                        [ "i 1", "R" ], [ "i 2", "S" ]
                       ],
                       [
                        [ "i 3", "P" ], [ "i 4", "S" ]
                       ]
                      ],
                      [
                       [
                        [ "i 5", "P" ], [ "i 6", "R" ]
                       ],
                       [
                        [ "i 7", "S" ], [ "i 8", "R" ]
                       ]
                      ]
                     ],

                     [
                      [
                       [
                        [ "i 9", "P" ], [ "i 10", "R" ]
                       ],
                       [
                        [ "i 11", "S" ], [ "i 12", "S" ]
                       ]
                      ],
                      [
                       [
                        [ "i 13", "P" ], [ "i 14", "S" ]
                       ],
                       [
                        [ "i 15", "R" ], [ "i 16", "S" ]
                       ]
                      ]
                     ]
                    ]
                   ]


    tournament16 = [
                    [[[[ [ "Ax", "R" ], ["Bx", "P"] ],
                       [ [ "Cx", "P"], ["Dx", "S"] ]],
                      [[ [ "Ex", "R" ], ["Fx", "P"] ],
                       [ [ "Gx", "P"], ["Hx", "S"] ]]],
                     [[[ [ "Ax2", "R" ], ["Bx2", "P"] ],
                       [ [ "Cx2", "P"], ["Dx2", "S"] ]],
                      [[ [ "Ex2", "R" ], ["Fx2", "P"] ],
                       [ [ "Gx2", "P"], ["Hx2", "S"] ] ]]],

                    [[[[ [ "Ax3", "R" ], ["Bx3", "P"] ],
                       [ [ "Cx3", "P"], ["Dx3", "S"] ]],
                      [[ [ "Ex3", "R" ], ["Fx3", "P"] ],
                       [ [ "Gx3", "P"], ["Hx3", "S"] ]]],
                     [[[ [ "Ax4", "R" ], ["Bx4", "P"] ],
                       [ [ "Cx4", "P"], ["Dx4", "S"] ]],
                      [[ [ "Ex4", "R" ], ["Fx4", "P"] ],
                       [ [ "Gx4", "P"], ["Hx4", "S"] ]]]]
                   ]

    rps_tournament_winner(tournament16)[0].should eq("Dx")
    rps_tournament_winner(tournament32)[0].should eq("Player 11")
  end
end
