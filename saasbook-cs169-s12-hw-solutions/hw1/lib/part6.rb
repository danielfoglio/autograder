class Numeric
  @@currencies = {'yen' => 0.013, 'euro' => 1.292, 'rupee' => 0.019, 'dollar' => 1}
  def method_missing(method_id)
    singular_currency = method_id.to_s.gsub( /s$/, '')
    if @@currencies.has_key?(singular_currency)
      self * @@currencies[singular_currency]
    else
      super
    end
  end

  def in(currency)
    self / 1.send(currency)
  end
end

class String
  def palindrome?
    std_str = self.downcase.gsub(/[^a-z]/, '')
    std_str.reverse == std_str
  end
end

module Enumerable
  def palindrome?
    array_form = self.collect{ |element| element }
    array_form == array_form.reverse
  end
end
