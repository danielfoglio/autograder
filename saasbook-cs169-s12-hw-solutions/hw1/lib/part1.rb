def palindrome?(str)
  std_str = str.downcase.gsub(/[^a-z]/, '')
  std_str.reverse == std_str
end

def count_words(str)
  split_str = str.scan /(\w+)/ rescue []
  word_list = split_str.flatten rescue []
  words = {}
  word_list.each { |word| words[word.downcase] = words[word.downcase].nil? ? 1 : words[word.downcase] + 1 }
  words
end
