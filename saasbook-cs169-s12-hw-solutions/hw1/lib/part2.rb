class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end

def rps_result(m1, m2)
  # returns true if player 1 wins, false if player 2 wins
  # assumes that m1 and m2 are already restricted to "R", "P", "S"
  return case m1
    when "R" then m2 != "P"
    when "P" then m2 != "S"
    when "S" then m2 != "R"
    end
end

def rps_game_winner(game)
  # Expects something like:
  # [ [ "Allen", "R" ], [ "Richard", "P" ] ]
  # => [ "Richard", "P" ]
  raise WrongNumberOfPlayersError unless game.length == 2

  raise NoSuchStrategyError unless
    game.map {|player| player[1]}.all? {
      |strategy| ["R", "P", "S"].include?(strategy.upcase()) }

  if rps_result(game[0][1], game[1][1])
    return game[0]
  else
    return game[1]
  end
end

def rps_tournament_winner(tournament)
  # Base case - is it just down to two players?
  if tournament.map { |player| player[0] }.all? { |e| e.is_a?(String) }
    return rps_game_winner(tournament)
  else
    return rps_game_winner(tournament.map { |t| rps_tournament_winner(t) })
  end
end
