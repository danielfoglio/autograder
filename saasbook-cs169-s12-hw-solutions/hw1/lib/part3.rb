# input: array of strings
# output: array of string arrays, grouped by anagrams (capital letters do not imply distinct anagrams, but case should be preserved in the output)
# the order of the subarrays and the elements in them don't matter
# example: ['cars', 'racs', 'four', 'for', 'scar', 'potatoes', 'creams', 'scream'] =>
#          [["cars", "racs", "scar"], ["four"], ["for"], ["potatoes"], ["creams", "scream"]]
#
# def combine_anagrams(words)
#   <YOUR CODE HERE>
# end

def combine_anagrams(words)
  word_map = {}
  words.each do |word|
    sorted_word = word.downcase.split('').sort.join
    if word_map[sorted_word]
      word_map[sorted_word] << word
    else
      word_map[sorted_word] = [word]
    end
  end

  word_map.values
end
