class CartesianProduct
  include Enumerable

  def initialize(a,b)
    @a = a
    @b = b
  end

  def each
    @a.each { |elementA|
      @b.each { |elementB|
        yield [elementA, elementB]
      }
    }
  end
end
