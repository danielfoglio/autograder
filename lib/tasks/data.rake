namespace :data do
  desc "Update the database using roster info.  Should not modify preexisting fields."
  task :update_from_roster => :environment do
    fil=Dir.glob("#{Rails.root}/lib/tasks/rosters/*.csv").sort.last
    file = File.open(fil)# comnsider option to open file with File.open(fil, 'rb') for windows endlines
    file.each do |line|
      array=[] #just in case
      array = line.split ','
      uu = User.create!(:username=> array[4],
        :email=> array[5].gsub("\n", ""),
        :password=> "!UnCc#{array[1]}$",
        :password_confirmation=> "!UnCc#{array[1]}$",
        :first_name=> array[2],
        :last_name=> array[0],
        :section=> array[3],
        :student_id=> array[1])

    end
  end

  desc 'add hw0'
  task :add_hw0 => :environment do
    a0 = Assignment.create! :number=>0, :description=>"Ruby Warm-up"
    1.upto(3).each do |n|
      problem = Problem.find_or_initialize_by_number_and_assignment_id(n, 1)
      problem.max_score = 100
      problem.save!
    end
  end


  desc 'add hw1'
  task :add_hw1 => :environment do
    if(!Assignent.find_by_number 1) 
      a1 = Assignment.create! :number=>1, :description=>"Ruby Calisthenics"

      1.upto(6).each do |n|
        problem = Problem.find_or_initialize_by_number_and_assignment_id(n,2 )
        problem.max_score = 100
        problem.save!
      end
    end
  end

  desc 'Add Ilson cci-1tcs3155autograder.uncc.edu, admin3155, and student'
  task :add_ilson_and_admin3155=> :environment do
    User.create!(:admin=>true, :password=>'!itcs!3155!', :first_name => 'Richard', :last_name => 'Ilson', :password_confirmation=>'!itcs!3155!', :username=>'rilson', :email=>'rilson@uncc.edu', :section=>0, :student_id=>0)
    User.create!(:admin=>true, :password=>'!itcs!3155!', :first_name => 'Admin', :last_name => 'Admin', :password_confirmation=>'!itcs!3155!', :username=>'admin3155', :email=>'itcsautograder@uncc.edu', :section=>0, :student_id=>0)
  end

  desc 'Copy without locking production into staging cci-1tcs3155autograder.uncc.edu'
  task :production_to_staging => :environment do 
    %x[cd #{Rails.root} && touch #{Rails.root}/db/pastsemesters/staging.db && chmod 777 #{Rails.root}/db/pastsemesters/staging.db]
    %x(sqlite3 #{Rails.root}/db/#{Rails.env}.sqlite3 '.backup #{Rails.root}/db/pastsemesters/staging.sqlite3')
  end

  desc 'Delete all data from database'
  task :backup_to_staging => :environment do 
    %x(RAILS_ENV=#{Rails.env} rake db:drop db:create db:migrate)
  end



  desc 'Update database with homework 2.'
  task :add_hw2 => :environment do
    if(!Assignent.find_by_number 2)
      a = Assignment.find_or_initialize_by_number 2
      a.description = 'Intro to Rails'
      a.save!

      1.upto(3).each do |n|
        problem = Problem.find_or_initialize_by_number_and_assignment_id(n, 3)
        problem.max_score = 100
        problem.save!
      end
    end
  end

  
  desc 'Add homework 3 in all its glory'
  task :add_hw3 => :environment do
    if(!Assignent.find_by_number 3)
      a = Assignment.find_or_initialize_by_number 3
      a.description = 'BDD & Cucumber'
      a.save!

      problem = Problem.find_or_initialize_by_number_and_assignment_id(1, 4)
      problem.max_score = 500
      problem.save!
    end
  end

  desc 'Add homework 4'
  task :add_hw4 => :environment do
    if(!Assignment.find_by_number 4)
      a = Assignment.find_or_initialize_by_number 4
      a.description = 'BDD and TDD Cycle'
      a.save!
      problem = Problem.find_or_initialize_by_number_and_assignment_id(1, 5)
      problem.max_score = 500
      problem.save!
    #a.problems.build(number: 1, max_score: 500)
    #a.save
  end
end
desc 'increment penalties 5% daily'
task :increment_penalties => :environment do
  Assignment.all.each do |ass|
    d = Deduction.find_by_assignment_id(ass)
    if d && d.penalty != 0 && d.penalty != 100
      d.penalty += 5
      d.save
    end
  end
end


end
namespace :assets do
  desc "Compile all the assets named in config.assets.precompile and push them"
  task :compile do
    AssetsCompiler.new.compile
  end

  class AssetsCompiler
    def compile
      ensure_clean_git
      removed_previous_assets
      compile_assets
      commit_compiled_assets
      push
    end

    def ensure_clean_git
      raise "Can't deploy without a clean git status." if git_dirty?
    end

    def removed_previous_assets
      run "bundle exec rake RAILS_ENV=production assets:clean"
    end

    def compile_assets
      run "bundle exec rake RAILS_ENV=production assets:precompile"
    end

    def commit_compiled_assets
      run "git add -u && git add . && git commit -am 'Compiled assets'"
    end

    def push
      run "git push"
    end

    private

    def run(command)
      puts "+ Running: #{command}"
      puts "-- #{system command}"
    end

    def git_dirty?
      `[[ $(git diff --shortstat 2> /dev/null | tail -n1) != "" ]]`
      dirty = $?.success?
    end
  end
end
